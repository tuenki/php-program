<?php
include_once "../php_conexion.php";
include_once "../funciones.php";

if(isset($_POST['id']))
{
    $id=$_POST['id'];
    $consulta="SELECT id,nombre,direccion,documento,seguro,telefono,edad,email,sexo,estado FROM pacientes WHERE id=$id";
    $c=mysqli_query($conexion,$consulta);
    $result=mysqli_fetch_row($c);
    $df=date_create($result[6]);
    $data = array(
        'id'=>$result[0],
        'nombre'=>$result[1],
        'direccion'=>$result[2],
        'documento'=>$result[3],
        'seguro'=>$result[4],
        'telefono'=>$result[5],
        'edad'=>date_format($df,'Y-m-d'),
        'email'=>$result[7],
        'sexo'=>$result[8],
        'estado'=>$result[9]
    );

    echo json_encode($data);
}
if(isset($_POST['IDCuaClin'])){
    $id=$_POST['IDCuaClin'];
    $consulta = "SELECT nombre,vih,peso,alergia,motivo,medicamento,enfermedad,enfermedadf,entrada from pacientes where id=$id";
    $c = mysqli_query($conexion,$consulta);
    $result = mysqli_fetch_row($c);
    $data = array(
        'nombre' => $result[0],
        'vih' => $result[1],
        'peso' => $result[2],
        'alergia' => $result[3],
        'motivo' => $result[4],
        'medicamento' => $result[5],
        'enfermedad' => $result[6],
        'enfermedadf' => $result[7],
        'entrada' => $result[8],
        );
    echo json_encode($data);
}
if(isset($_POST['idD']))
{
    $id=$_POST['idD'];
    $consulta="SELECT id,nombre FROM pacientes WHERE id=$id";
    $c=mysqli_query($conexion,$consulta);
    $result=mysqli_fetch_row($c);
    $data = array(
        'id'=>$result[0],
        'nombre'=>$result[1]
    );

    echo json_encode($data);
}
if(isset($_POST['idP']))
{
    $id=$_POST['idP'];
    $consulta="SELECT id,nombre,direccion,telefono,edad FROM pacientes WHERE id=$id";
    $c=mysqli_query($conexion,$consulta);
    $result=mysqli_fetch_row($c);
    $df=date_create($result[4]);
    $data = array(
        'id'=>$result[0],
        'nombre'=>$result[1],
        'direccion'=>$result[2],
        'telefono'=>$result[1],
        'fecha'=>date_format($df,'Y-m-d'),
        'edad'=>CalculaEdad(date_format($df,'Y-m-d')),
        'visita' => mysqli_num_rows($c)
    );

    echo json_encode($data);
}
@mysqli_free_result($result);
@mysqli_close($conexion);
?>