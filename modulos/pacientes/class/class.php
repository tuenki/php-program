<?php
class Proceso_Paciente{
	var $conexion;
	var $id;
	var $imagen;	
	var $documento;
	var $seguro;
	var $nombre;
    var $direccion;		
	#var $departamento;	
	#var $municipio;		
	var $telefono;		
	var $edad;		
	var $sexo;		
	var $email;	
	var $estado;
	var $id_consultorio;
	var $vih;
	var $peso;
	var $alergia;
	var $motivo;
	var $medicamento;
	var $enfermedad;
	var $enfermedadf;
	var $entrada;

	function __construct($conexion,$id,$imagen,$documento,$seguro,$nombre,$direccion,$telefono,$edad,$sexo,$email,$estado,$id_consultorio,$vih,$peso,$alergia,$motivo,$medicamento,$enfermedad,$enfermedadf,$entrada){
		$this->conexion=$conexion;
		$this->id=$id;		
		$this->imagen=$imagen;
		$this->documento=$documento;
		$this->seguro=$seguro;		
		$this->nombre=$nombre;		
		$this->direccion=$direccion;	
		$this->telefono=$telefono;
		$this->edad=$edad;	
		$this->sexo=$sexo;	
		$this->email=$email;			
		$this->estado=$estado;	
		$this->id_consultorio=$id_consultorio;	
		$this->vih=$vih;	
		$this->peso=$peso;	
		$this->alergia=$alergia;	
		$this->motivo=$motivo;	
		$this->medicamento=$medicamento;	
		$this->enfermedad=$enfermedad;	
		$this->enfermedadf=$enfermedadf;	
		$this->entrada=$entrada;	
	}
	
	function crear(){
		$conexion=$this->conexion;
		$id=$this->id;
		$imagen=$this->imagen;		
		$documento=$this->documento;
		$seguro=$this->seguro;		
		$nombre=$this->nombre;		
		$direccion=$this->direccion;	
		$telefono=$this->telefono;	
		$edad=$this->edad;	
		$sexo=$this->sexo;	
		$email=$this->email;	
		$estado=$this->estado;	
		$id_consultorio=$this->id_consultorio;	
		$vih=$this->vih;	
		$peso=$this->peso;	
		$alergia=$this->alergia;	
		$motivo=$this->motivo;	
		$medicamento=$this->medicamento;	
		$enfermedad=$this->enfermedad;	
		$enfermedadf=$this->enfermedadf;	
		$entrada=$this->entrada;	
							
		mysqli_query($conexion,"INSERT INTO pacientes (imagen, documento, seguro, nombre, direccion, telefono, edad, sexo, email, estado, consultorio, vih, peso, alergia, motivo, medicamento, enfermedad, enfermedadf, entrada) 
					VALUES ('$imagen','$documento','$seguro','$nombre','$direccion','$telefono','$edad','$sexo','$email','$estado','$id_consultorio','$vih','$peso','$alergia','$motivo','$medicamento','$enfermedad','$enfermedadf','$entrada')");
	}
	
	function actualizar(){
		$conexion=$this->conexion;
		$id=$this->id;
		$imagen=$this->imagen;		
		$documento=$this->documento;
		$seguro=$this->seguro;		
		$nombre=$this->nombre;		
		$direccion=$this->direccion;	
		#$departamento=$this->departamento;
		#$municipio=$this->municipio;
		$telefono=$this->telefono;	
		$edad=$this->edad;	
		$sexo=$this->sexo;	
		$email=$this->email;	
		$estado=$this->estado;		
		
		mysqli_query($conexion,"UPDATE pacientes SET
										imagen='$imagen',
										documento='$documento',
										seguro='$seguro',  
										nombre='$nombre',
										direccion='$direccion',
										telefono='$telefono',
										edad='$edad',
										sexo='$sexo',										
										email='$email',
										estado='$estado'
									WHERE id='$id'");
	}
}
?>