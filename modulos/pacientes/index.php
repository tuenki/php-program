<?php 
	session_start();
	include_once "../php_conexion.php";
	include_once "class/class.php";
	include_once "../funciones.php";
	include_once "../class_buscar.php";
	include_once "../consultas_medicas/class/class.php";
	include_once "../citas_medicas/class/class.php";
	include_once "vcards/VCardParser.php";
	use JeroenDesloovere\VCard\VCardParser;

	if($_SESSION['cod_user']){
	}else{
		header('Location: ../../php_cerrar.php');
	}
	
	$usu=$_SESSION['cod_user'];
	$pa=mysqli_query($conexion,"SELECT * FROM cajero WHERE usu='$usu'");				
	while($row=mysqli_fetch_array($pa)){
		$id_consultorio=$row['consultorio'];
		$oConsultorio=new Consultar_Deposito($conexion,$id_consultorio);
		$nombre_Consultorio=$oConsultorio->consultar('nombre');
	}
	
	$oPersona=new Consultar_Cajero($conexion,$usu);
	$cajero_nombre=$oPersona->consultar('nom');
	$fecha=date('Y-m-d');
	$hora=date('H:i:s');
	
	######### TRAEMOS LOS DATOS DE LA EMPRESA #############
		$pa=mysqli_query($conexion,"SELECT * FROM empresa WHERE id=1");				
        if($row=mysqli_fetch_array($pa)){
			$nombre_empresa=$row['empresa'];
		}
	

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $nombre_empresa; ?></title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- CALENDARIO STYLES-->
	<link href="../../assets/todo/bootstrap-datetimepicker.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="../../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="../../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../usuarios/perfil.php"><?php echo $_SESSION['user_name']; ?></a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;">Consultorio: <?php echo $nombre_Consultorio; ?> : Fecha de Acceso : <?php echo fecha(date('Y-m-d')); ?> &nbsp; <a href="../../php_cerrar.php" class="btn btn-danger square-btn-adjust">Salir</a> </div>
        </nav>   
           <?php include_once "../../menu/m_pacientes.php"; ?>
        <div id="page-wrapper" >
            <div id="page-inner">
				<div class="panel-body">                                              
				   <?php
	//form actualizar y nuevo 
	if(isset($_POST['actualizarBTN'])){
		@$id=limpiar($_POST['id']);
		@$imagen=addslashes(file_get_contents($_FILES['imagen']['tmp_name']));
		@$documento=limpiar($_POST['documento']);
		@$seguro=limpiar($_POST['seguro']);		
		@$nombre=limpiar($_POST['nombre']);		
		@$direccion=limpiar($_POST['direccion']);
		@$telefono=limpiar($_POST['telefono']);
		@$edad=limpiar($_POST['edad']);			
		@$sexo=limpiar($_POST['sexo']);															
		@$email=limpiar($_POST['email']);															
		@$estado=limpiar($_POST['estado']);
		#ANTECEDENTES DESDE AQUI
		@$vih=limpiar($_POST['vih']);
		@$peso=limpiar($_POST['peso']);					
		@$alergia=limpiar($_POST['alergia']);
		@$motivo=limpiar($_POST['motivo']);				
		@$medicamento=limpiar($_POST['medicamento']);
		@$enfermedad=limpiar($_POST['enfermedad']);
		@$enfermedadf=limpiar($_POST['enfermedadf']);
		@$entrada=limpiar($_POST['entrada']);						
		if(empty($id)){
			$documento=$usu;
			$oPaciente=new Proceso_Paciente($conexion,'',$imagen,$documento,$seguro,$nombre,$direccion,$telefono,$edad,$sexo,$email,$estado,$id_consultorio,
																		$vih,$peso,$alergia,$motivo,$medicamento,$enfermedad,$enfermedadf,$entrada);
			$oPaciente->crear();
			echo mensajes('Paciente "'.$nombre.'" Registrado con Exito','verde');
		}else{
			
			if(!empty($imagen)) {
				$oPaciente=new Proceso_Paciente($conexion,$id,$imagen,$documento,$seguro,$nombre,$direccion,$telefono,$edad,$sexo,$email,$estado,$id_consultorio,
																		$vih,$peso,$alergia,$motivo,$medicamento,$enfermedad,$enfermedadf,$entrada);
				$oPaciente->actualizar();
				echo mensajes('Paciente "'.$nombre.'" Actualizado con Exito','verde');
				}
				else{
					$query="UPDATE pacientes SET
					documento='$documento',
					seguro='$seguro',  
					nombre='$nombre',
					direccion='$direccion',
					telefono='$telefono',
					edad='$edad',
					sexo='$sexo',										
					email='$email',
					estado='$estado' WHERE id = '$id'";
					mysqli_query($conexion,$query);
					echo mensajes('Paciente "'.$nombre.'" Actualizado con Exito2','verde');
			}
									
		}
	}
	if(isset($_POST['CuaCli'])){
		#ANTECEDENTES DESDE AQUI
		@$id=limpiar($_POST['id']);
		@$vih=limpiar($_POST['vih']);
		@$peso=limpiar($_POST['peso']);					
		@$alergia=limpiar($_POST['alergia']);
		@$motivo=limpiar($_POST['motivo']);				
		@$medicamento=limpiar($_POST['medicamento']);
		@$enfermedad=limpiar($_POST['enfermedad']);
		@$enfermedadf=limpiar($_POST['enfermedadf']);
		@$entrada=limpiar($_POST['entrada']);
		mysqli_query($conexion,"UPDATE pacientes SET
				vih='$vih',
				peso='$peso',
				alergia='$alergia',
				motivo='$motivo',
				medicamento='$medicamento',
				enfermedad='$enfermedad',
				enfermedadf='$enfermedadf',
				entrada='$entrada'																			
				WHERE id=$id
				");	
				echo mensajes('Expedinte Registrado con Exito','verde');
	}
	
	//variables de form cita
	if(isset($_POST['ncita'])){
		@$id=$_POST['idC'];
		@$nom=$_POST['nombreP'];
		@$medico=limpiar($_POST['medico']);
		@$fec=limpiar($_POST['fcita']);
		@$hor=limpiar($_POST['horario']);
		@$observaciones=limpiar($_POST['observaciones']);
		$fecha=date('Y-m-d');
		$hora=date('H:m:s');

		$sql = "INSERT INTO citas_medicas (id_paciente, id_medico, consultorio, fechai, observaciones, fecha,hora,horario,status,consulta)
			VALUES ('$id','$medico','$id_consultorio','$fec','$observaciones','$fecha','$hora','$hor','PENDIENTE','PENDIENTE')";
		if(mysqli_query($conexion,$sql)){
			echo mensajes('Cita Medica de '.$nom.' guardada con Exito','verde');
		}
		else {

			echo mensajes('Se produjo un error'. mysqli_error($conexion).'','rojo');
		}											 
	}


	if(isset($_POST['eliminarU']))
	{
		$id=$_POST['idEl'];
		$del="DELETE FROM pacientes WHERE id=$id";	
		if(mysqli_query($conexion,$del))
		{
			echo mensajes('Paciente Eliminado con Exito','rojo');
		}
	}
	

				   ?>                
				  <!--  Modals-->
								 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<form name="form1" method="post" enctype="multipart/form-data" action="">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													
														<h3 align="center" class="modal-title" id="myModalLabel">Nuevo Paciente</h3>
													</div>
										<div class="panel-body">
										<div class="row">
										 <ul class="nav nav-tabs nav-justified">
                                            <li class="active"><a href="#personal" data-toggle="tab"><i class="glyphicon glyphicon-user" ></i> DATOS PERSONALES</a></li>
                                            <li class="" ><a href="#cuadro" data-toggle="tab"><i class="glyphicon glyphicon-list" ></i> CUADRO CLINICO</a></li>                                                                                                                                                                                                                               
                                         </ul>
                                         	<div class="tab-content">
                                         	<div class="tab-pane fade active in" id="personal">									
											<div class="col-md-12">
											<br>																																																									
											<strong>Fotografia</strong><br>
								
											<input type="file" name="imagen" id="imagen"></br>	
                                            <input class="form-control" title="Se necesita un nombre"  name="nombretitular" placeholder="Nombre del Titular" autocomplete="off" required autofocus><br>
                                            <input class="form-control" title="Se necesita un nombre"  name="nombre" placeholder="Nombre del Paciente" autocomplete="off" required autofocus><br>
											<input class="form-control" title="Se necesita un nombre"  name="nombretutor" placeholder="Nombre del padre,madre o tutor" autocomplete="off" required autofocus><br>       
                                            <input class="form-control" title="Se necesita una Direccion" name="direccion" placeholder="Dirección"  autocomplete="off" required><br>
                                            <input class="form-control" title="Se necesita una Direccion" name="direccionrfc" placeholder="Domicilio del RFC"  autocomplete="off" required><br>
                                            
                                            </div>
											<div class="col-md-6">
												<input class="form-control" name="rfc" placeholder="R.F.C"  autocomplete="off" required><br>
                                                <input class="form-control" name="poliza" placeholder="No. de Poliza"  autocomplete="off" required><br>
                                                <input class="form-control" name="nocertificado" placeholder="No. de Certificado"  autocomplete="off" required><br>
												<input class="form-control" name="telefono" title="Se necesita un Telefono" data-mask="9999999999" placeholder="Telefono Casa" autocomplete="off" required><br>
                                                
												<input class="form-control" name="email" placeholder="Email" autocomplete="off"><br>
                                                <select class="form-control" name="sexo" autocomplete="off" required>
													<option value="" selected disabled>--SEXO--</option>
													<option value="m">Masculino</option>
													<option value="f">Femenino</option>													
												</select><br>	
											</div>
											<div class="col-md-6">
												<!--<input class="form-control" name="edad" title="Se necesita una Edad" pattern="^[0-9.!#$%&'*+/=?^_`{|}~-]*$" placeholder="Edad" autocomplete="off" required><br>
												<input placeholder="Fecha de Nacimiento" type="text" onfocus="(this.type='date')"  class="form-control" name="edad" min="1"  autocomplete="off" required><br>-->									
												<select class="form-control" name="seguro" autocomplete="off" required>																					
												<option value="" selected disabled>--SEGURO--</option>
													<?php
															$p=mysqli_query($conexion,"SELECT * FROM seguros WHERE estado='s'");				
															while($r=mysqli_fetch_array($p)){
																echo '<option value="'.$r['id'].'">'.$r['nombre'].'</option>';
															}
														?>												
												</select><br>    
												<div class='input-group date form_date' id='form_date' data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
													<input type='text' name="edad" id="form_date" class="form-control" placeholder="Fecha de Nacimiento" onfocus="(this.type='')" required autocomplete="off" />
													<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
												</div>
												<input type="hidden" id="dtp_input2" name="edad" /><br>											
												<input class="form-control" name="telefonoc" title="Se necesita un Telefono" data-mask="9999999999" placeholder="Telefono Celular" autocomplete="off" required><br>																							 
												<select class="form-control" name="estado" placeholder="Estado" autocomplete="off" required>						
													<option value="s">Activo</option>
													<option value="n">No Activo</option>													
												</select>
											</div>
											</div>
											<div class="tab-pane fade" id="cuadro"> 
                                                <br>
                                            <div class="col-md-12">											
												<div class="input-group">
												  <span class="input-group-addon">Es alergico a alguna substancia o medicamento: </span>
												  <select class="form-control" name="vih" autocomplete="off" required>
													<option value="" selected disabled>---SELECCIONE---</option>
													<option value="s">Si</option>
													<option value="n">No</option>																									
												</select>
												</div><br>
                                                
                                                <div class="input-group">
    											  <span class="input-group-addon">Consume algun medicamento: </span>
												  <select class="form-control" name="consume_medicamento" autocomplete="off" required>
													<option value="" selected disabled>---SELECCIONE---</option>
													<option value="s">Si</option>
													<option value="n">No</option>																									
												</select>
												</div><br>
                                                
                                                <div class="input-group">
        										  <span class="input-group-addon">Presenta problemas con anestesia local o general: </span>
												  <select class="form-control" name="problemas_anestesia" autocomplete="off" required>
													<option value="" selected disabled>---SELECCIONE---</option>
													<option value="s">Si</option>
													<option value="n">No</option>																									
												</select>
												</div><br>
                                                
                                                <div class="input-group">
        										  <span class="input-group-addon">Padece presion alta o baja: </span>
												  <select class="form-control" name="padece de presion alta o baja" autocomplete="off" required>
													<option value="" selected disabled>---SELECCIONE---</option>
													<option value="a">Alta</option>
													<option value="b">Baja</option>
                                                    <option value="n">No</option>
												</select>
												</div><br>
                                                
                                                <div class="input-group">
        										  <span class="input-group-addon">Padece enfermedades Cardiovasculares:</span>
												  <select class="form-control" name="enfermedades_cardiovasculares" autocomplete="off" required>
													<option value="" selected disabled>---SELECCIONE---</option>
													<option value="s">Si</option>
													<option value="n">No</option>																									
												</select>
												</div><br>
                                                
                                                <div class="input-group">
            									  <span class="input-group-addon">Es usted alergico a la penicilina:</span>
												  <select class="form-control" name="alergico_penicilina" autocomplete="off" required>
													<option value="" selected disabled>---SELECCIONE---</option>
													<option value="s">Si</option>
													<option value="n">No</option>																									
												</select>
												</div><br>
                                                
                                                <div class="input-group">
                								  <span class="input-group-addon">Padecio alguna enfermedad grave recientemente:</span>
												  <select class="form-control" name="padecio_enfermedad" autocomplete="off" required>
													<option value="" selected disabled>---SELECCIONE---</option>
													<option value="s">Si</option>
													<option value="n">No</option>																									
												</select>
												</div><br>
                                                
                                                
                                                 <div class="input-group">
            									  <span class="input-group-addon">Es alergico a alguna substancia o medicamento</span>
												  <select class="form-control" name="alergico_substancia" autocomplete="off" required>
													<option value="" selected disabled>---SELECCIONE---</option>
													<option value="s">Si</option>
													<option value="n">No</option>																									
												</select>
												</div><br>
                                                
										    	<div class="input-group"> 
												<span class="input-group-addon">Peso</span>
    											 <input class="form-control" name="peso" autocomplete="off" required><br>
												</div><br>												
											<!--	<span class="input-group-addon">Alergias Medicamentosas:</span>
                                                <textarea class="form-control" name="alergia" rows="2"></textarea><br>	-->														
												<span class="input-group-addon">Motivo de Consulta:</span>
                                                <textarea class="form-control" name="motivo" rows="2"></textarea><br>
											</div> 
                                            
										<!--	<div class="col-md-6"> 											
												
                                                
											<!--	 <span class="input-group-addon">Antecedentes Patalógicos (APP):</span>
                                                <textarea class="form-control" name="enfermedad" rows="2"></textarea><br>
                                                <span class="input-group-addon">Antecedentes Patalógicos (APF):</span>
                                                <textarea class="form-control" name="enfermedadf" rows="2"></textarea><br>
											     <div class="input-group">                                 
                                       		      <span class="input-group-addon">Entrada</span>
												  <select class="form-control" name="entrada" autocomplete="off" required>
													<option value="" selected disabled>---SELECCIONE---</option>
													<option value="Emergencia">Emergencia</option>
													<option value="Consulta">Consulta</option>
													<option value="Interconsulta">Interconsulta</option> 																							
												</select>    
												</div><br>     
											</div>     -->


											</div> 																																												                                                            
											</div> 																																												                                                            
										</div> 
										</div> 
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-primary" id="actualizarBTN" name="actualizarBTN">Guardar</button>
                                        </div>										 
                                    </div>
                                </div>
								</form>
                            </div>
							
							 <!--  Modals cuadro clinico-->
							 <div class="modal fade" id="cuadrocli" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<form name="form3" method="post" action="">
										<input type="hidden" value="" name="id" id="IDCuCl">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													
														<h3 align="center" class="modal-title" id="myModalLabel">Cuadro Clinico</h3>
													</div>
										<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
											<div class="alert alert-info" align="center"><div id="NombreCuCl"></div></div>
																									
											</div>
											<div class="col-md-6">												
												<div class="input-group">
												  <span class="input-group-addon">VIH</span>
												  <select id="VIHCuCl" class="form-control" name="vih" autocomplete="off" required>
													<option value="" selected disabled>---SELECCIONE---</option>
													<option value="p">Positivo</option>
													<option value="n">Negativo</option>																									
												</select>
												</div><br>
												<div class="input-group">
												  <span class="input-group-addon">Peso</span>
												 <input class="form-control" name="peso" id="PesoCuCl" autocomplete="off" required><br>
												</div><br>												
												<span class="input-group-addon">Alergias Medicamentosas:</span>
                                                <textarea class="form-control" name="alergia"  id="AlergiasCuCl" rows="2"></textarea><br>																
												<span class="input-group-addon">Motivo de Consulta:</span>
                                                <textarea class="form-control" name="motivo"  id="MotivoCuCl" rows="2"></textarea><br>
											</div>
											<div class="col-md-6">												
												<span class="input-group-addon">Uso de Medicamentos:</span>
                                                <textarea class="form-control" name="medicamento" id="MedicamentoCuCl"  rows="2"></textarea><br>
												<span class="input-group-addon">Antecedentes Patalógicos (APP):</span>
                                                <textarea class="form-control" name="enfermedad" id="EnfermedadCuCl"  rows="2"></textarea><br>
                                                <span class="input-group-addon">Antecedentes Patalógicos (APF):</span>
                                                <textarea class="form-control" name="enfermedadf" id="EnfermedadFCuCl"  rows="2"></textarea><br>
											     <div class="input-group">                                 
                                       		      <span class="input-group-addon">Entrada</span>
												  <select class="form-control" name="entrada" id="EntradaCuCl" autocomplete="off" required>
													<option value="" selected disabled>---SELECCIONE---</option>
													<option value="Emergencia">Emergencia</option>
													<option value="Consulta">Consulta</option>
													<option value="Interconsulta">Interconsulta</option>																									
												</select>
												</div><br>
											</div>

										</div> 
										</div> 
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            <button type="submit" name="CuaCli" class="btn btn-primary">Guardar</button>
                                        </div>
                                    </div>
                                </div>
								</form>
                            </div>
                     <!-- End Modals-->  
					  <!-- Modal eliminar -->           			
					  <div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<form action="" method="post">
								<input type="hidden" id="id" name="idEl">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>													
													<h3 align="center" class="modal-title" id="myModalLabel">Seguridad</h3>
											</div>
											<div class="panel-body">
												<div class="row" align="center">
												<div class="col-md-4"></div>
												<div class="col-md-4">
													<label id="nombre" name="nombre"></label>
													
												</div>
												<div class="col-md-4"></div>
												                                     
																										
													<br><br>
													<div class="alert alert-danger">
													<h4>¿Esta Seguro de Realizar esta Acción?<br><br> 
														una vez Eliminado el paciente<br> 
														no podran ser Recuperados sus datos.<br>
														No recomendamos esta accion, sino la de "Activo" o No Activo, porque de este
														depende mucha informcion en el Almacen de datos.
													</h4>
													</div>																																																																																																								
												</div> 
											</div> 
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
												<button type="submit" class="btn btn-danger" name="eliminarU">Eliminar</button>																
											</div>										 
										</div>
									</div>
								</form>
						</div>
					 <!-- End Modals-->	 		

					<!--  Modals cita-->
					<div class="modal fade" id="citas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<form name="form2" method="post" action="">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										
                                            <h3 alegn="center" class="modal-title" id="myModalLabel">Nueva Cita</h3>
                                        </div>
										<div class="panel-body">
										<div class="row">                                       
											<div class="col-md-6">
									
												<label>Paciente:</label>
												<input type="hidden" id="idC" name="idC">
												<input type="text" name="nombreP" id="nombreP" class="form-control" value="" placeholder="Nombre Paciente">												
												<br>
												<span class="input-group-addon">Asignar Medico:</span>
												 <select class="form-control" name="medico" id="medico" autocomplete="off" required>			
													<?php
														$p=mysqli_query($conexion,"SELECT doc, nombre FROM usuario WHERE estado='s'");				
														while($r=mysqli_fetch_array($p)){
															if($r['doc']==$usu){
																echo '<option value="'.$r['doc'].'" selected>'.$r['nombre'].'</option>';
															}else{
																echo '<option value="'.$r['doc'].'">'.$r['nombre'].'</option>';
															}
														}
													?>										
												</select>
												<br>												        			
												
												             																																
											</div>
											
											<div class="col-md-6">
											<br>
											<div class='input-group date form_date' id="form_datex" data-date="" data-date-format="yyyy-mm-dd" data-link-format="yyyy-mm-dd">
													<input type='text' name="fcita" id="form_datex" class="form-control" value="" placeholder="Fecha" required/>				
													<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
											</div>
											<br>
												<div class='input-group date form_time' id='form_time' data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
													<input type='text' name="horario" id="form_time" class="form-control" placeholder="Horario" onfocus="(this.type='')" required autocomplete="off" />
													<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
												</div><br/>  
												   												
												<!--<input type="date" class="form-control" name="fechai" min="1"  autocomplete="off" required><br>-->																			
												<label>Observaciones:</label>
												<textarea class="form-control" name="observaciones" rows="3"></textarea><br>
											</div>                                                                        
										</div> 
										</div> 
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-primary" id="ncita" name="ncita">Guardar</button>
                                        </div>										 
                                    </div>
                                </div>
								</form>
                            </div>
                     <!-- End Modals-->
                            <!--  Modals actualizar-->
							<div class="modal fade" id="actualizar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												<form name="form1" method="post" enctype="multipart/form-data" action="">
												<input type="hidden" id="idA" name="id" value="">
												<input type="hidden" name="vih">
												<input type="hidden" name="peso">
												<input type="hidden" name="alergia">
												<input type="hidden" name="motivo">
												<input type="hidden" name="medicamento">
												<input type="hidden" name="enfermedad">
												<input type="hidden" name="enfermedadf">
												<input type="hidden" name="entrada">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
															
																<h3 align="center" class="modal-title" id="myModalLabel">Actualizar</h3>
															</div>
										<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
											<br>

											<input type="file" name="imagen" id="imagen"></br>	

											<div class="input-group">
												  <span class="input-group-addon">Nombre</span>
												  <input class="form-control" title="Se necesita un nombre" id="nombreA" name="nombre" placeholder="Nombre Completo" value="" autocomplete="off" required><br>											
											</div><br>
											<div class="input-group">
												  <span class="input-group-addon">Direccion</span>
												  <input class="form-control" title="Se necesita un nombre" id="direccion" name="direccion" placeholder="Dirección" value="" autocomplete="off" required><br>											
											</div><br>											
											</div>
											<div class="col-md-6">																				
												<div class="input-group">
												  <span class="input-group-addon">Cédula:</span>
												  <input class="form-control" id="documento" name="documento"   autocomplete="off" required><br>											
											    </div><br>	
												<div class="input-group">
												  <span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></span>
												 <input class="form-control" id="telefono" name="telefono"  autocomplete="off" required ><br>
												</div><br>
												<div class="input-group">
												  <span class="input-group-addon">Sexo</span>
												  <select class="form-control" id="sexo" name="sexo" autocomplete="off" required>
													<option value="m">Masculino</option>
													<option value="f">Femenino</option>												
												</select>												
												</div><br>	
											</div>
											<div class="col-md-6">
											<div class="input-group">
												  <span class="input-group-addon">Seguro:</span>
												 <select class="form-control" id="seguro" name="seguro" onchange="pais(this.value);" autocomplete="off" required>			
													<?php
														$p=mysqli_query($conexion,"SELECT * FROM seguros WHERE estado='s'");				
														while($r=mysqli_fetch_array($p)){
															if($r['id']==$row['seguros']){
																echo '<option value="'.$r['id'].'" selected>'.$r['nombre'].'</option>';
															}else{
																echo '<option value="'.$r['id'].'">'.$r['nombre'].'</option>';
															}
														}
													?>										
												</select>											
												</div><br>																							
												<div class='input-group date form_date' id="form_dateU" data-date="" data-date-format="yyyy-mm-dd" data-link-format="yyyy-mm-dd">
													<span class="input-group-addon">Nac:</span>
													<input type='text' name="edad" id="edad" class="form-control" value="" required/>				
													<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
												</div><br>
												
												<div class="input-group">
												  <span class="input-group-addon">@</span>
												  <input class="form-control" id="email" name="email" autocomplete="off" value=""><br>												
												</div><br>
												<div class="input-group">
												  <span class="input-group-addon">Estado</span>
												  <select class="form-control" id="estado" name="estado" autocomplete="off" required>
													<option value="s">Activo</option>
													<option value="n">No Activo</option>													
												</select>												
												</div>
											</div>                                 
                                       
										</div> 
										</div> 
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-primary" id="actualizarBTN" name="actualizarBTN">Actualizar</button>
                                        </div>										 
                                    </div>
                                </div>
								</form>
                            </div>
                     <!-- End Modals-->
                            <div class="modal fade" id="myModalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<form name="form1" method="post" enctype="multipart/form-data" action="">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													
														<h3 align="center" class="modal-title" id="myModalLabel">Agregar pasientes con vCards</h3>
													</div>
										<div class="panel-body">
										<div class="row">
											 <ul class="nav nav-tabs nav-justified">
                                            <li class="active"><a href="#archivo" data-toggle="tab"><i class="glyphicon glyphicon-file" ></i> Archivo vCard</a></li>
                                            <li class="" ><a href="#carpeta" data-toggle="tab"><i class="glyphicon glyphicon-folder-open" ></i> Carpeta con archivos vCards</a></li>                                                                                                                                                                                                                               
                                         </ul>
										 
                                         	<div class="tab-content">
                                         	<div class="tab-pane fade active in" id="archivo">									
											<div class="col-md-6">

												<br>																																																									
												<strong class="input-group-addon">Archivo vCard (.vcf)</strong><br>
											
											
												<input type="file" name="vcard" id="vcard" accept=".vcf"></br>
                                            
                                            </div>
													
											<div class="col-md-6">
												<br>
											<span class="input-group-addon">Asignar Medico:</span>
												 <select class="form-control" name="medico" id="medico" autocomplete="off" required>			
													<?php
														$p=mysqli_query($conexion,"SELECT doc, nombre FROM usuario WHERE estado='s'");				
														while($r=mysqli_fetch_array($p)){
															if($r['doc']==$usu){
																echo '<option value="'.$r['doc'].'" selected>'.$r['nombre'].'</option>';
															}else{
																echo '<option value="'.$r['doc'].'">'.$r['nombre'].'</option>';
															}
														}
													?>										
												</select>									
												
											</div>
											</div>
											<div class="tab-pane fade" id="carpeta"> 
																			<div class="col-md-6">

												<br>																																																									
												<strong class="input-group-addon">Carpeta vCard (.vcf) <br> Predeterminado: archivo </strong><br>
											
											
												<input class="form-control" type="text" name="dircarpeta" id="dircarpeta" value="archivo" ></br>
                                            
                                            </div>																														</div>						                                                            
											</div> 					                                    
										</div> 
										</div> 
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            <button name="vcardbtn" type="submit" class="btn btn-primary">Guardar</button>
                                        </div>										 
                                    </div>
                                </div>
								</form>
                            </div>
                     <!-- End Modals-->
			<?php
			
				//consulta
				if(!empty($_POST['id_paciente'])){

					$id_paciente=limpiar($_POST['id_paciente']);
					$sintomas=limpiar($_POST['sintomas']);
					$examen=limpiar($_POST['examen']);																												
					$diagnostico=limpiar($_POST['diagnostico']);																											
					$tratamiento=limpiar($_POST['tratamiento']);																											
					$reseta='ok';																											
					$observaciones=limpiar($_POST['observaciones']);
					$fecha=date('Y-m-d');
					//$hora=date('H:i:s');
					$status='PENDIENTE';
			#lOS MEDICAMENTOS
					$med1=limpiar($_POST['med1']);										
					$indi1=limpiar($_POST['indi1']);
					@$med2=limpiar($_POST['med2']);										
					@$indi2=limpiar($_POST['indi2']);
					@$med3=limpiar($_POST['med3']);										
					@$indi3=limpiar($_POST['indi3']);
					@$med4=limpiar($_POST['med4']);										
					@$indi4=limpiar($_POST['indi4']);
					@$med5=limpiar($_POST['med5']);										
					@$indi5=limpiar($_POST['indi5']);
					@$med6=limpiar($_POST['med6']);										
					@$indi6=limpiar($_POST['indi6']);
					@$med7=limpiar($_POST['med7']);										
					@$indi7=limpiar($_POST['indi7']);
					@$med7=limpiar($_POST['med7']);										
					@$indi7=limpiar($_POST['indi7']);
					@$med8=limpiar($_POST['med8']);										
					@$indi8=limpiar($_POST['indi8']);
					@$med9=limpiar($_POST['med9']);										
					@$indi9=limpiar($_POST['indi9']);
					@$med10=limpiar($_POST['med10']);										
					@$indi10=limpiar($_POST['indi10']);	
															
				if(empty($_POST['id'])){
					$status='PROCESADO';
					$oConsulta=new Proceso_Consulta($conexion,'',$id_paciente,$usu,$id_consultorio,$sintomas,$examen,$diagnostico,$tratamiento,$reseta,$observaciones,$fecha,$hora,$status,
													$med1,$indi1,$med2,$indi2,$med3,$indi3,$med4,$indi4,$med5,$indi5,$med6,$indi6,$med7,$indi7,$med8,$indi8,$med9,$indi9,$med10,$indi10);
					$oConsulta->crear();
					echo mensajes('Consulta Medica Creada con Exito','verde');
				}else{
					$id=limpiar($_POST['id']);
					
					$oConsulta=new Proceso_Consulta($conexion,$id,$sintomas,$examen,$diagnostico,$tratamiento,$reseta,$observaciones);
					$oConsulta->actualizar();
					echo mensajes('Consulta Medica Actualizada con Exito','verde');
				}
			}
			
			if(isset($_POST['cuadro'])){
				@$id=limpiar($_POST['id']);
				#$sangre=limpiar($_POST['sangre']);						
				@$vih=limpiar($_POST['vih']);
				@$peso=limpiar($_POST['peso']);					
				#$talla=limpiar($_POST['talla']);
				@$alergia=limpiar($_POST['alergia']);
				@$motivo=limpiar($_POST['motivo']);				
				@$medicamento=limpiar($_POST['medicamento']);
				@$enfermedad=limpiar($_POST['enfermedad']);
				@$enfermedadf=limpiar($_POST['enfermedadf']);
				@$entrada=limpiar($_POST['entrada']);												
				mysqli_query($conexion,"UPDATE pacientes SET
												vih='$vih',
												peso='$peso',
												alergia='$alergia',
												motivo='$motivo',
												medicamento='$medicamento',
												enfermedad='$enfermedad',
												enfermedadf='$enfermedadf',
												entrada='$entrada'																			
										WHERE id=$id
				");	
				echo mensajes('Expedinte Registrado con Exito','verde');
			}
			?>
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
							PACIENTES
							<ul class="nav pull-right">
								<a href="" class="btn btn-default btn-xs" data-toggle="modal" data-target="#myModal" title="Agregar Paciente" title="Agregar"><i class="fa fa-plus"> </i> <strong>Nuevo</strong></a>
								<a href="" class="btn btn-default btn-xs" data-toggle="modal" data-target="#myModalv" title="Agregar Pacientes por VCards" title="Agregar"><i class="fa fa-plus"> </i> <strong>Importar (VCards)</strong></a>							                            																										                            
							</ul>
						</div>
                        <div class="panel-body">
						<div class="col-md-8"></div>
                            <div class="col-md-4">
								<section>
									<i class="glyphicon glyphicon-search"></i>
									<input class="form-control" type="text" name="busqueda" id="busqueda" placeholder="Buscar...">
								</section>
							</div>							

							<section id="tabla_resultado">
								<!-- AQUI SE DESPLEGARA NUESTRA TABLA DE CONSULTA -->
							</section>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->
				<!--  Modals consulta-->
				<div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<form name="form1" method="post" action="">
						<input type="hidden"  id="id_paciente" name="id_paciente" value="">
							<div class="modal-dialogx">
								<div class="modal-content">
									<div class="panel-body">
										<div class="row">
											<ul class="nav nav-tabs nav-justified">
	                                        	<li class="active"><a href="#datos" data-toggle="tab"><i class="glyphicon glyphicon-book" ></i> CONSULTA</a></li>
	                                        	<li class="" ><a href="#tipo" data-toggle="tab"><i class="glyphicon glyphicon-book" ></i> RECETA/MEDICAMENTOS</a></li>                                                                                                                                                                                     
	                                        </ul><br>
	                                        <div class="tab-content">
	                                            <div class="tab-pane fade active in" id="datos">
	                                            	<div class="col-md-6">
														<label>Nombre:&nbsp;</label><label id="nombreB"></label><br>													
														<label>Dirección:&nbsp;</label><label id="direccionB"></label> <br>												
														<label>Telefono:&nbsp;</label><label id="telefonoB"></label> <br>
													</div>
													<div class="col-md-6">		
														<label>Fecha de Nacimiento:&nbsp;</label><label id="fechaB"></label><br>														
														<label>Edad:&nbsp;</label><label id="edadB"></label><br>
														<label>Visitas:&nbsp; <span class="label label-success"></span></label><label id="visitasB"></label><br><br>
													</div> 																																		   
													<div class="col-md-6">																						
														<span class="input-group-addon">Motivo de consulta:</span>
														<textarea class="form-control" name="sintomas" rows="3" required></textarea><br>
														<span class="input-group-addon">Examen Físico:</span>
														<textarea class="form-control" name="examen" rows="3" required></textarea><br>												
														<span class="input-group-addon">Diagnostico:</span>
														<textarea class="form-control" name="diagnostico" rows="3" required></textarea><br>
													</div>
													<div class="col-md-6">																				
														<span class="input-group-addon">Tratamiento:</span>
														<textarea class="form-control" name="tratamiento" rows="3" required></textarea><br>
														<span class="input-group-addon">Nota:</span>
														<textarea class="form-control" name="observaciones" rows="3"></textarea><br>																									
													</div>
												</div>
												<div class="tab-pane fade" id="tipo">
                                                    <div class="col-md-6">											
														<input class="form-control" name="med1" placeholder="Medicamento 1" autocomplete="off" required>
                                                   	 	<textarea class="form-control" name="indi1" placeholder="Indicación" rows="2" required></textarea><br>
                                                    	<input class="form-control" name="med2" placeholder="Medicamento 2" autocomplete="off">
                                                    	<textarea class="form-control" name="indi2" placeholder="Indicación" rows="2" ></textarea><br>
                                                    	<input class="form-control" name="med3" placeholder="Medicamento 3" autocomplete="off">
                                                    	<textarea class="form-control" name="indi3" placeholder="Indicación" rows="2" ></textarea><br>
                                                    	<input class="form-control" name="med4" placeholder="Medicamento 4" autocomplete="off">
                                                    	<textarea class="form-control" name="indi4" placeholder="Indicación" rows="2"></textarea><br>
                                                    	<input class="form-control" name="med5" placeholder="Medicamento 5" autocomplete="off">
                                                    	<textarea class="form-control" name="indi5" placeholder="Indicación" rows="2"></textarea><br>
													</div>
													<div class="col-md-6">																				
														<input class="form-control" name="med6" placeholder="Medicamento 6" autocomplete="off">
                                                    	<textarea class="form-control" name="indi6" placeholder="Indicación" rows="2"></textarea><br>
                                                    	<input class="form-control" name="med7" placeholder="Medicamento 7" autocomplete="off">
                                                    	<textarea class="form-control" name="indi7" placeholder="Indicación" rows="2" ></textarea><br>
                                                    	<input class="form-control" name="med8" placeholder="Medicamento 8" autocomplete="off">
                                                    	<textarea class="form-control" name="indi8" placeholder="Indicación" rows="2"></textarea><br>
                                                    	<input class="form-control" name="med9" placeholder="Medicamento 9" autocomplete="off">
                                                    	<textarea class="form-control" name="indi9" placeholder="Indicación" rows="2"></textarea><br>
                                                    	<input class="form-control" name="med10" placeholder="Medicamento 10" autocomplete="off">
                                                    	<textarea class="form-control" name="indi10" placeholder="Indicación" rows="2"></textarea><br>																							
													</div> 
												</div>
											</div>                                                                         																																												 																																													 
										</div> 
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
											<button type="submit" class="btn btn-primary" id="NConsulta" name="NConsulta">Guardar</button>
										</div>										 
								</div>
							</div>
					</form>
				</div>
					<!-- End Modals-->
				 
					 
        </div>               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="../../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<!-- CALENDARIO SCRIPTS -->
    <script src="../../assets/todo/bootstrap-datetimepicker.js"></script>
    <script src="../../assets/todo/locales/bootstrap-datetimepicker.es.js"></script>
	<!-- VALIDACIONES -->
	<script src="peticion.js"></script>
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
    <!-- DATATIMEPICKER -->
   <script  src="../../assets/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
   <script  src="../../assets/js/locales/bootstrap-datetimepicker.es.js" charset="UTF-8"></script>
   <script type="text/javascript">
	$(function () {
		$('#form_dateU').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
		
           $('#form_date').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
	$('#form_time').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0
    });
    $('#form_datex').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
    $('#form_timex').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0
    });		
        });
	
   </script>
   <script type="text/javascript">
 	function actualizarpaciente(id)
	{
		$.ajax({
			url:'obtener.php',
			type:'POST',
			data:'id='+id,
			success:function(r){
			datos=jQuery.parseJSON(r);
				$('#idA').val(datos['id']);
				$('#nombreA').val(datos['nombre']);
				$('#direccion').val(datos['direccion']);
				$('#documento').val(datos['documento']);
				$('#telefono').val(datos['telefono']);
				$('#sexo').val(datos['sexo']);
				$('#seguro').val(datos['seguro']);
				$('#edad').val(datos['edad']);
				$('#email').val(datos['email']);
				$('#estado').val(datos['estado']);
			}		
		})	
	
	} 
	function eliminarpaciente(id)
	{
		$.ajax({
			url:'obtener.php',
			type:'POST',
			data:'idD='+id,
			success:function(r){
			datos=jQuery.parseJSON(r);
				$('#id').val(datos['id']);
				$('#nombre').text(datos['nombre']);
			}		
		})	
	}
	function citapaciente(id)
	{
		$.ajax({
			url:'obtener.php',
			type:'POST',
			data:'idD='+id,
			success:function(r){
			datos=jQuery.parseJSON(r);
				$('#idC').val(datos['id']);
				$('#nombreP').val(datos['nombre']);
			}		
		})	
	} 
	function consultapaciente(id)
	{
		$.ajax({
			url:'obtener.php',
			type:'POST',
			data:'idP='+id,
			success:function(r){
			datos=jQuery.parseJSON(r);
				$('#id_paciente').val(datos['id']);
				$('#nombreB').text(datos['nombre']);
				$('#direccionB').text(datos['direccion']);
				$('#telefonoB').text(datos['telefono']);
				$('#fechaB').text(datos['fecha']);
				$('#edadB').text(datos['edad']);
				$('#visitasB').text(datos['visita']);
			}		
		})

	}
	function MostrarCuadroClinico(id)
	{
		$.ajax({
			url:'obtener.php',
			type:'POST',
			data:'IDCuaClin='+id,
			success:function(r){
			datos=jQuery.parseJSON(r);
			$('#IDCuCl').val(id);
			$('#NombreCuCl').html("<strong>"+datos['nombre']+"</strong>");
			$('#VIHCuCl').val(datos['vih']);
			$('#PesoCuCl').val(datos['peso']);
			$('#AlergiasCuCl').val(datos['alergia']);
			$('#MotivoCuCl').val(datos['motivo']);
			$('#MedicamentoCuCl').val(datos['medicamento']);
			$('#EnfermedadCuCl').val(datos['enfermedad']);
			$('#EnfermedadFCuCl').val(datos['enfermedadf']);
			$('#EntradaCuCl').val(datos['entrada']);
			}

		})
	}
   </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>       
</body>
</html>

