<?php
/////// CONEXIÓN A LA BASE DE DATOS /////////
include_once "../php_conexion.php";
include_once "../funciones.php";

//////////////// VALORES INICIALES ///////////////////////

$tabla="";
$signo="'";
///////// LO QUE OCURRE AL TECLEAR SOBRE EL INPUT DE BUSQUEDA ////////////

if(isset($_POST['nombre']))
{
	$q=$conexion->real_escape_string($_POST['nombre']);
	$query="SELECT * FROM pacientes WHERE 
		nombre LIKE '%".$q."%'";
}
@$buscarP=$conexion->query($query);
if (@$buscarP->num_rows > 0)
{
	$tabla.= 
	'<br>
	<table class="table">
		<tr class="bg">
			<th>NOMBRE</th>
			<th>TELEFONO</th>
			<th>CONSULTA</th>
			<th>CITA</th>
			<th></th>
		</tr>';

	while($filaP= $buscarP->fetch_assoc())
	{
		$tabla.=
		'<tr>
			<td><a  href="../perfil_paciente/index.php?id='.$filaP['id'].'"><i class="fa fa-user fa-2x"></i>'.$filaP['nombre'].' </a></td>
			<td>'.$filaP['telefono'].'</td>
			<td class="center">
				<button class="btn btn-primary" data-toggle="modal" data-target="#new" onclick="consultapaciente('.$signo.$filaP['id'].$signo.')"><i class="fa fa-list-alt"></i></button>
			</td>
			<td class="center">
				<button class="btn btn-info" data-toggle="modal" data-target="#citas" onclick="citapaciente('.$signo.$filaP['id'].$signo.');"><i class="fa fa-list"></i></button>
			</td>
			<td class="center">
				<div class="btn-group">
				<button data-toggle="dropdown" class="btn btn-warning btn-sm dropdown-toggle"><i class="fa fa-cog"></i> <span class="caret"></span></button>
				<ul class="dropdown-menu pull-right">
					<li><a  href="../perfil_paciente/index.php?id='.$filaP['id'].'"><i class="fa fa-user"></i> Perfil</a></li>
					<li class="divider"></li>
					<li><a href="#" data-toggle="modal" data-target="#cuadrocli" onclick="MostrarCuadroClinico('.$filaP['id'].');"><i class="fa fa-list"></i>Cuadro Clinico</a></li>
					<li class="divider"></li>
					<li><a  href="#" data-toggle="modal" data-target="#actualizar" onclick="actualizarpaciente('.$signo.$filaP['id'].$signo.');"><i class="fa fa-edit"></i> Editar</a></li>
					<li class="divider"></li>
					<li><a href="#" data-toggle="modal" data-target="#eliminar" onclick="eliminarpaciente('.$signo.$filaP['id'].$signo.');" ><i class="fa fa-pencil"></i> Eliminar</a></li>																																				
					</ul>
				</div>																				
				</td>
		 </tr>
		';					 
	}

	$tabla.='</table>';
} else
	{
		$tabla="No se encontraron coincidencias con sus criterios de búsqueda.";
	}
	echo $tabla;
?>
