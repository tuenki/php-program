<?php 
	session_start();
	include_once "../php_conexion.php";
	include_once "class/class.php";
	include_once "../funciones.php";
	include_once "../class_buscar.php";
	if($_SESSION['cod_user']){
	}else{
		header('Location: ../../php_cerrar.php');
	}
	
	$usu=$_SESSION['cod_user'];
	$pa=mysqli_query($conexion,"SELECT * FROM cajero WHERE usu='$usu'");				
	while($row=mysqli_fetch_array($pa)){
		$id_consultorio=$row['consultorio'];
		$oConsultorio=new Consultar_Deposito($conexion,$id_consultorio);
		$nombre_Consultorio=$oConsultorio->consultar('nombre');
	}
	
	$oPersona=new Consultar_Cajero($conexion,$usu);
	$cajero_nombre=$oPersona->consultar('nom');
	$fecha=date('Y-m-d');
	$hora=date('H:i:s');
	
	######### TRAEMOS LOS DATOS DE LA EMPRESA #############
		$pa=mysqli_query($conexion,"SELECT * FROM empresa WHERE id=1");				
        if($row=mysqli_fetch_array($pa)){
			$nombre_empresa=$row['empresa'];
		}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $nombre_empresa; ?></title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="../../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="../../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../usuarios/perfil.php"><?php echo $_SESSION['user_name']; ?></a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;">Consultorio: <?php echo $nombre_Consultorio; ?> :: Fecha de Acceso : <?php echo fecha(date('Y-m-d')); ?> &nbsp; <a href="../../php_cerrar.php" class="btn btn-danger square-btn-adjust">Salir</a> </div>
        </nav>   
           <?php include_once "../../menu/m_departamento.php"; ?>
        <div id="page-wrapper" >
            <div id="page-inner">						                				 
                 <div class="panel-body" align="right">                                                                                 
                            <button type="button" class="btn btn-success btn-circle" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus fa-2x"></i>
                            </button>                                                                              
                  </div>
				  <!--  Modals-->
                     <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<form name="form1" method="post" action="">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										
                                            <h3 align="center" class="modal-title" id="myModalLabel">Nuevo Provedor</h3>
                                        </div>
										<div class="panel-body">
										<div class="row">                                       
											<div class="col-md-6">											
												<label>Nombre:</label>
												<input class="form-control" name="nombre" autocomplete="off" required><br>											
											</div>
											
											<div class="col-md-6">											
												<label>Telefono:</label>
												<input class="form-control" name="telefono" autocomplete="off" required><br>											
											</div>
											<div class="col-md-6">											
												<label>Celular:</label>
												<input class="form-control" name="celular" autocomplete="off" required><br>											
											</div>
											<div class="col-md-6">											
												<label>Direccion:</label>
												<input class="form-control" name="direccion" autocomplete="off" required><br>											
											</div>
											<div class="col-md-6">											
												<label>Correo:</label>
												<input class="form-control" name="Correo" autocomplete="off" required><br>											
											</div>
										                                                                        
										</div> 
										</div> 
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                        </div>										 
                                    </div>
                                </div>
								</form>
                            </div>
                     <!-- End Modals-->

					 <!-- Modal Actualizar -->
					 <div class="modal fade" id="actualizar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<form name="form1" method="post" action="">
						<input type="hidden" id="idPA" name="id">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									
										<h3 align="center" class="modal-title" id="myModalLabel">Actualizar Provedores</h3>
									</div>
									<div class="panel-body">
									<div class="row">                                       
										<div class="col-md-6">											
											<label>Nombre:</label>
											<input class="form-control" id="NombrePA" name="nombre" autocomplete="off" required ><br>											
										</div>
												
										<div class="col-md-6">											
											<label>Telefono:</label>
											<input class="form-control" id="TelPA" name ="telefono" autocomplete="off" required ><br>											
										</div>

										<div class="col-md-6">											
											<label>Celular:</label>
											<input class="form-control" id="CelPA" name="celular" autocomplete="off" required ><br>											
										</div>
										<div class="col-md-6">											
											<label>Direccion:</label>
											<input class="form-control" id="DirPA" name="direccion" autocomplete="off" required ><br>											
										</div>
					
										<div class="col-md-6">											
											<label>Correo:</label>
											<input class="form-control" id="EmPA" name="Correo" autocomplete="off" required ><br>											
										</div>                                                             
									</div> 
									</div> 
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
										<button type="submit" class="btn btn-primary">Guardar</button>
									</div>										 
								</div>
							</div>
							</form>
						</div>

					 <!-- Modal Actualizar fin -->

					 <!-- Modal Eliminar -->
					 <div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													<form  action="" method="post">
													<input type="hidden" name="idDF" id="IDD" >
													<div class="modal-dialog">
														<div class="modal-content">
																		<div class="modal-header">
																			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>													
																			<h3 align="center" class="modal-title" id="myModalLabel">Seguridad</h3>
																		</div>
															<div class="panel-body">
															<div class="row" align="center">                                       
																										
																<strong>Hola! <?php echo $cajero_nombre; ?></strong><br><br>
																<div class="alert alert-danger">
																	<h4>¿Esta Seguro de Realizar esta Acción?<br> 
																	una vez Eliminado el provedor <div id="NombreD"></div> no podra ser Recuperado.</h4>
																</div>																																																																																																								
															</div> 
															</div> 
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
																<button type="submit"  class="btn btn-danger" name ="EliminarProv" title="Eliminar">Eliminar</button>
																				
															</div>										 
														</div>
													</div>
													</form>
												</div>
					<!-- Fin Modal Eliminar -->
					 
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                             Provedores
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<?php 
									if(!empty($_POST['nombre'])){ 												
										$nombre=limpiar($_POST['nombre']);
										$telefono=limpiar($_POST['telefono']);
										$celular=limpiar($_POST['celular']);
										$direccion=limpiar($_POST['direccion']);																											
										$email=limpiar($_POST['Correo']);
										
										if(empty($_POST['id'])){
											$oProvedor=new Proceso_Provedores($conexion,'',$nombre,$telefono,$celular,$direccion,$email);
											$oProvedor->crear();
											echo mensajes('Provedor "'.$nombre.'" Creado con Exito','verde');
										}else{
											$id=limpiar($_POST['id']);
											$oProvedor=new Proceso_Provedores($conexion,$id,$nombre,$telefono,$celular,$direccion,$email);
											$oProvedor->actualizar();
											echo mensajes('Provedor "'.$nombre.'" Actualizado con Exito','verde');
										}
									}
									if(isset($_POST['EliminarProv'])){
										$id=$_POST['idDF'];
										mysqli_query($conexion,"DELETE FROM provedores WHERE id='$id'");
										echo mensajes('Se elimino el provedor correctamente','rojo');
									}

								?>
                                
								
                            </div>
							<div class="row">
								<div class="col-xs-6 col-md-2">
									
								</div>
								<div class="col-xs-12 col-md-8">
									<div class="form-group form-group-lg">
									<b>
										<input type="text" name="busqueda" id="busqueda" class="form-control" placeholder="Buscar provedor..."></b>
										
									</div>
								</div>
							</div>
                            <section id="tabla_resultado">
								<!-- AQUI SE DESPLEGARA NUESTRA TABLA DE CONSULTA -->
							</section>
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->                                     
        </div>               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="../../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<script src="peticion.js"></script>
	<!-- VALIDACIONES -->
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
    
<script>
function ActualizarProvedores(id){
	$.ajax({
			url:'obtener.php',
			type:'POST',
			data:'id='+id,
			success:function(r){
			datos=jQuery.parseJSON(r);
				$('#idPA').val(datos['id']);
				$('#NombrePA').val(datos['name']);
				$('#TelPA').val(datos['phone']);
				$('#CelPA').val(datos['cel']);
				$('#DirPA').val(datos['addres']);
				$('#EmPA').val(datos['email']);
			}		
		})

}
function EliminarProvedores(id){
	$.ajax({
			url:'obtener.php',
			type:'POST',
			data:'idE='+id,
			success:function(r){
			datos=jQuery.parseJSON(r);
				$('#IDD').val(datos['id']);
				$('#NombreD').html("<strong>"+datos['name']+"</strong>");
			}
		})
}
</script>
</body>
</html>
