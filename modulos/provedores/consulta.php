<?php
/////// CONEXIÓN A LA BASE DE DATOS /////////
include_once "../php_conexion.php";
include_once "../funciones.php";

//////////////// VALORES INICIALES ///////////////////////

$tabla="";
$signo="'";

//Busqueda por nombre de provedor///

if(isset($_POST['nombre'])){
    $q=$conexion->real_escape_string($_POST['nombre']);
	$query="SELECT * FROM provedores WHERE 
		name LIKE '%".$q."%'";
}
@$buscarP=$conexion->query($query);
if (@$buscarP->num_rows > 0)
{
    $tabla.= 
	'<table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    
    <thead>
        <tr>
            <th>NOMBRE</th>
            <th>TELEFONO</th>
            <th>CELULAR</th>
            <th>DIRECCION</th> 
            <th>CORREO</th>                                                                                     
            <th>ACCIONES</th>
        </tr>
    </thead>';
    while($filaP= $buscarP->fetch_assoc())
	{
        $tabla.=
        '<tr>
            <td>'.$filaP['name'].'</td>
            <td> '.$filaP['phone'].'</td>
            <td>'.$filaP['cel'].' </td>
            <td>'.$filaP['addres'].' </td>
            <td>'.$filaP['email'].' </td>
            <td class="center">
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><i class="fa fa-cog"></i> <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                    <li><a href="#" data-toggle="modal" data-target="#actualizar" onclick="ActualizarProvedores('.$filaP['id'].');"><i class="fa fa-edit"></i> Editar</a></li>
                    <li class="divider"></li>
                    <li><a href="#" data-toggle="modal" data-target="#eliminar" onclick="EliminarProvedores('.$filaP['id'].');"><i class="fa fa-pencil"></i> Eliminar</a></li>																																				
                    </ul>
                </div>											
            </td>
        
        
        </tr>';
    }

    
    $tabla.= '</table>';
}
else{
    $tabla ="";
}
echo $tabla;

?>
