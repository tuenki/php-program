<?php 
	session_start();
	include_once "../php_conexion.php";
	include_once "class/class.php";
	include_once "../funciones.php";
	include_once "../class_buscar.php";
	if($_SESSION['cod_user']){
	}else{
		header('Location: ../../php_cerrar.php');
	}
	$id_medico=$_SESSION['cod_user'];
	$usu=$_SESSION['cod_user'];
	$oPersona=new Consultar_Cajero($conexion,$usu);
	$cajero_nombre=$oPersona->consultar('nom');
	
	$fechay=date('Y-m-d');
	$horay=date('H:m:s');
	
	$usu=$_SESSION['cod_user'];
	$pa=mysqli_query($conexion,"SELECT * FROM cajero WHERE usu='$usu'");				
	while($row=mysqli_fetch_array($pa)){
		$id_consultorio=$row['consultorio'];
		$oConsultorio=new Consultar_Deposito($conexion,$id_consultorio);
		$nombre_Consultorio=$oConsultorio->consultar('nombre');
	}
	
	######### TRAEMOS LOS DATOS DE LA EMPRESA #############
		$pa=mysqli_query($conexion,"SELECT * FROM empresa WHERE id=1");				
        if($row=mysqli_fetch_array($pa)){
			$nombre_empresa=$row['empresa'];
		}
		######### TRAEMOS LOS DATOS DE CONSULTORIO #############
		$pa=mysqli_query($conexion,"SELECT * FROM consultorios WHERE id=$id_consultorio");				
        if($row=mysqli_fetch_array($pa)){
			$nombre_medico=$row['encargado'];
			
		}		
	
	if(!empty($_GET['del'])){
		$id=$_GET['del'];
		mysqli_query($conexion,"DELETE FROM citas_medicas WHERE status='PENDIENTE' and id='$id'");
		header('index.php');
		
	}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $nombre_empresa; ?></title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- CALENDARIO STYLES-->
   <link href="../../assets/todo/bootstrap-datetimepicker.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="../../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="../../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../usuarios/perfil.php"><?php echo $_SESSION['user_name']; ?></a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;">Consultorio: <?php echo $nombre_Consultorio; ?> :: Fecha de Acceso : <?php echo fecha(date('Y-m-d')); ?> &nbsp; <a href="../../php_cerrar.php" class="btn btn-danger square-btn-adjust">Salir</a> </div>
        </nav>   
           <?php include_once "../../menu/m_citas_medica.php"; ?>
        <div id="page-wrapper" >
            <div id="page-inner">						                									 
				  <!--  Modals-->
                     <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<form name="form1" method="post" action="">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										
                                            <h3 align="center" class="modal-title" id="myModalLabel">Nueva Cita</h3>
                                        </div>
										<div class="panel-body">
										<div class="row">                                       
											<div class="col-md-6">									
												<label>Paciente:</label>												
												<select class="form-control" name="id_paciente" required>
												<option value="" selected disabled>---SELECCIONE---</option>
                                                	<?php
														$sal=mysqli_query($conexion,"SELECT * FROM pacientes WHERE consultorio='$id_consultorio' and estado='s'");				
														while($col=mysqli_fetch_array($sal)){
															echo '<option value="'.$col['id'].'">'.$col['nombre'].'</option>';
														}
													?>
                                                </select><br>												        			
												<div class='input-group date form_date' id='form_date' data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
													<input type='text' name="fechai" id="form_date" class="form-control" placeholder="Fecha Proxima Cita" onfocus="(this.type='')" required autocomplete="off" />
													<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
												</div>
												<input type="hidden" id="dtp_input2" name="fechai" /><br/>              																																
											</div>
											<div class="col-md-6">
												<div class='input-group date form_time' id='form_time' data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
													<input type='text' name="horario" id="form_time" class="form-control" placeholder="Horario" onfocus="(this.type='')" required autocomplete="off" />
													<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
												</div><br/>       												
												<!--<input type="date" class="form-control" name="fechai" min="1"  autocomplete="off" required><br>-->																			
												<label>Observaciones:</label>
												<textarea class="form-control" name="observaciones" rows="3"></textarea><br>
											</div>                                                                        
										</div> 
										</div> 
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                        </div>										 
                                    </div>
                                </div>
								</form>
                            </div>
                     <!-- End Modals-->
					 
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            CITAS
							<ul class="nav pull-right">
								<a href="" class="btn btn-default btn-xs" data-toggle="modal" data-target="#myModal" title="Agregar Paciente" title="Agregar"><i class="fa fa-plus"> </i> <strong>Nueva</strong></a>								                            																										                            
							</ul>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
								<?php 
									if(!empty($_POST['id_paciente'])){ 												
										$id_paciente=limpiar($_POST['id_paciente']);																											
										$fechai=limpiar($_POST['fechai']);																																																						
										$horario=limpiar($_POST['horario']);																																																						
										$observaciones=limpiar($_POST['observaciones']);
										$fecha=date('Y-m-d');
										$hora=date('H:m:s');
										$status='PENDIENTE';
										$consulta='PENDIENTE';
										
																						
										if(empty($_POST['id'])){
											$oConsulta=new Proceso_Cita($conexion,'',$id_paciente,$id_medico,$id_consultorio,$fechai,$observaciones,$fecha,$hora,$horario,$status,$consulta);
											$oConsulta->crear();
											echo mensajes('Cita Medica Guardada con Exito','verde');
										}else{
											$id=limpiar($_POST['id']);
											$oConsulta=new Proceso_Cita($conexion,$id,$fechai,$observaciones,$horario);
											$oConsulta->actualizar();
											echo mensajes('Cita Medica Actualizada con Exito','verde');
										}
									}
								?>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    
									<thead>
                                        <tr>
                                            <th>#</th>
                                            <th>PACIENTE</th>                                                                                                                              
                                            <th>FECHA DE PROXIMA CITA</th>                                                                                      
                                            <th>MED/ASIST.</th>                                                                                      
                                            <th>STATUS</th>                                                                                      
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php 
											if(!empty($_POST['buscar'])){
												$buscar=limpiar($_POST['buscar']);
												$pame=mysqli_query($conexion,"SELECT * FROM citas_medicas WHERE consultorio='$id_consultorio'");	
											}else{
												$pame=mysqli_query($conexion,"SELECT * FROM citas_medicas WHERE consultorio='$id_consultorio'");		 
											}		
											while($row=mysqli_fetch_array($pame)){
											$oPaciente=new Consultar_Paciente($conexion,$row['id_paciente']);
							                #$oMedico=new Consultar_Medico($row['id_medico']);
											$url=$row['id'];
											
											############# STATUS FULL ######################
											if($row['status']=='PENDIENTE'){
												
												 $status='<span class="label label-warning">PENDIENTE</span>';
											}else{
												$status='<span class="label label-success">ATENDIDO</span>';
												
											}
											$pamela=strftime( "%Y-%m-%d-%H-%M-%S", time() );										
											if($row['fechai']==$pamela){
													$status='si';
												}																								
												elseif($row['fechai']>$pamela){
													$status='<span class="label label-danger">PENDIENTE</span>';
												}
												$horario=$row['horario'];
										?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $row['id']; ?></td>
                                            <td><i class="fa fa-user fa-2x"></i> <?php echo $oPaciente->consultar('nombre'); ?></td>                                                                                   
                                            <td><?php echo fecha($row['fechai']).' '.$row['horario']; ?></td>
											<td><?php echo $nombre_medico; ?></td>  
											<td><?php echo $status; ?></td>  
                                            <td class="center">
											<div class="btn-group">
											  <button data-toggle="dropdown" class="btn btn-warning btn-sm dropdown-toggle"><i class="fa fa-cog"></i> <span class="caret"></span></button>
											  <ul class="dropdown-menu pull-right">
												<li><a href="#" data-toggle="modal" data-target="#actualizar<?php echo $row['id']; ?>"><i class="fa fa-edit"></i> Editar</a></li>
												<li class="divider"></li>
												<li><a  href="#" data-toggle="modal" data-target="#eliminar<?php echo $row['id']; ?>"><i class="fa fa-pencil"></i> Eliminar</a></li>																																				
											  </ul>
											</div>
											</td>
								
                                        </tr> 
										
										<!--  Modals-->
										 <div class="modal fade" id="actualizar<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												<form name="form1" method="post" action="">
												<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
															
																<h3 align="center" class="modal-title" id="myModalLabel">Modificar Cita</h3>
															</div>
															<div class="panel-body">
															<div class="row">                                       
																<div class="col-md-6">
																	<label>Paciente:</label>
																		<select class="form-control" name="id_paciente" disabled >
																			<option value="x">---SELECCIONE---</option>
																			<?php
																				$p=mysqli_query($conexion,"SELECT * FROM pacientes WHERE estado='s'");				
																				while($r=mysqli_fetch_array($p)){
																					if($r['id']==$row['id_paciente']){
																						echo '<option value="'.$r['id'].'" selected>'.$r['nombre'].'</option>';
																					}else{
																						echo '<option value="'.$r['id'].'">'.$r['nombre'].'</option>';
																					}
																				}
																			?>
																		</select><br>
																		<div class="input-group date form_time" id='form_timex' data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
																			 <span class="input-group-addon">Hora:</span>
																			<input class="form-control" type="text" id='form_timex' name="horario"  value="<?php echo $row['horario']; ?>" required>
																			<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
																		</div><br/>	
																		<input type="hidden" id="dtp_input3" name="horario" /><br/>																																																						
																		</div>
																<div class="col-md-6">
																		<div class='input-group date form_date' id='form_datex' data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
																			<span class="input-group-addon">Fecha:</span>
																			<input type='text' name="fechai" id="form_datex" class="form-control" value="<?php echo $row['fechai']; ?>" required/>				
																			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																		</div>
																		<input type="hidden" id="dtp_input2" name="fechai" /><br/>  																		
																		<label>Observaciones:</label>
																		<textarea class="form-control" name="observaciones" value="<?php echo $row['observaciones']; ?>" rows="3"><?php echo $row['observaciones']; ?></textarea><br>											
																</div>                                                                        
															</div> 
															</div> 
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
																<button type="submit" class="btn btn-primary">Guardar</button>
															</div>										 
														</div>
													</div>
													</form>
												</div>
										 <!-- End Modals-->
										 <!-- Modal -->           			
												<div class="modal fade" id="eliminar<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">													
													<form name="contado" action="index.php?del=<?php echo $row['id']; ?>" method="get">													
													<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
													<div class="modal-dialog">
														
														<div class="modal-content">
																		<div class="modal-header">
																			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>													
																			<h3 align="center" class="modal-title" id="myModalLabel">Seguridad</h3>
																		</div>
															<div class="panel-body">
															<div class="row" align="center">                                       
																										
																<strong>Hola! <?php echo $cajero_nombre; ?></strong><br><br>
																<div class="alert alert-danger">
																	<h4>¿Esta Seguro de Realizar esta Acción?<br><br> 
																	una vez Eliminada la cita Medica con fecha <strong>[ <?php echo fecha($row['fecha']); ?> ]</strong><br>
																	con paciente <strong>[ <?php echo $oPaciente->consultar('nombre'); ?> ]</strong>
																	no podran ser Recuperados sus datos.<br>
																	Tenga en cuenta que si la cita fue procesada no podra ser eliminada.
																	</h4>
																</div>																																																																																																								
															</div> 
															</div> 
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
																<a href="index.php?del=<?php echo $row['id']; ?>"  class="btn btn-danger" title="Eliminar">
																	<i class="fa fa-times" ></i> <strong>Eliminar</strong>
																</a>																
															</div>										 
														</div>
											
													</div>
													</form>
													
												</div>
										 <!-- End Modals-->       	
										
										
											<?php } ?>
                                    </tbody>
									
                                </table>
								
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->
				
        </div>               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="../../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<!-- CALENDARIO SCRIPTS -->
    <script src="../../assets/todo/bootstrap-datetimepicker.js"></script>
    <script src="../../assets/todo/locales/bootstrap-datetimepicker.es.js"></script>
	<!-- VALIDACIONES -->
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
    <!-- DATATIMEPICKER -->
   <script  src="../../assets/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
   <script  src="../../assets/js/locales/bootstrap-datetimepicker.es.js" charset="UTF-8"></script>
   <script type="text/javascript">
        $(function () {
           $('#form_date').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
	$('#form_time').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0
    });
    $('#form_datex').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
    $('#form_timex').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0
    });		
        });
   </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>  
</body>
</html>
