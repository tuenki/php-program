,<?php 
	session_start();
	include_once "../php_conexion.php";
	include_once "class/class.php";
	include_once "../funciones.php";
	include_once "../class_buscar.php";
	include_once "../consultas_medicas/class/class.php";
	if(!empty($_GET['id'])){
		$factura=$_GET['id'];
	}else{
		header('Location:error.php');
	}
	if($_SESSION['cod_user']){
		$usu=limpiar($_SESSION['cod_user']);
		
		$usu=$_SESSION['cod_user'];
	$pa=mysqli_query($conexion,"SELECT * FROM cajero WHERE usu='$usu'");				
	while($row=mysqli_fetch_array($pa)){
		$id_consultorio=$row['consultorio'];
		$oConsultorio=new Consultar_Deposito($conexion,$id_consultorio);
		$nombre_Consultorio=$oConsultorio->consultar('nombre');
	}
		
		$pa=mysqli_query($conexion,"SELECT * FROM pacientes WHERE consultorio='$id_consultorio' and id='$factura'");				
		if($row=mysqli_fetch_array($pa)){
			if(is_null($row['imagen'])){
				$imagen = "vacio";
			}else{
				$imagen=$row['imagen'];
			}
			$idP=$row['id'];			
			$nombre=$row['nombre'];
			$direccion=$row['direccion'];
			$telefono=$row['telefono'];
			$departamento=$row['departamento'];
			$municipio=$row['municipio'];
			$edad=$row['edad'];
			$sexo=$row['sexo'];
			$email=$row['email'];
			$sangre=$row['sangre'];
			$vih=$row['vih'];
			$peso=$row['peso'];
			$talla=$row['talla'];
			$alergia=$row['alergia'];
			$medicamento=$row['medicamento'];
			$enfermedad=$row['enfermedad'];			
			$estado=$row['estado'];			
			$oDepto=new Consultar_Departamento($conexion,$departamento);
			$oMcpio=new Consultar_Municipio($conexion,$municipio);
		}else{
			header('Location:error.php');
		}
		
	}
	
	$usu=$_SESSION['cod_user'];
	
	$oPersona=new Consultar_Cajero($conexion,$usu);
	$cajero_nombre=$oPersona->consultar('nom');
	$fecha=date('Y-m-d');
	
	$hora=date('H:i:s');
	
	$pa=mysqli_query($conexion,"SELECT * FROM cajero WHERE usu='$usu'");				
	while($row=mysqli_fetch_array($pa)){
		$id_bodega=$row['consultorio'];
		$oDeposito=new Consultar_Deposito($conexion,$id_bodega);
		$nombre_deposito=$oDeposito->consultar('nombre');
	}
	######### TRAEMOS LOS DATOS DE CONSULTORIO #############
		$pax=mysqli_query($conexion,"SELECT * FROM consultorios WHERE id=$id_consultorio");				
        if($row=mysqli_fetch_array($pax)){
			$nombre_medico=$row['encargado'];
			
		}	
	######### TRAEMOS LOS DATOS DE LA EMPRESA #############
		$pa=mysqli_query($conexion,"SELECT * FROM empresa WHERE id=1");				
        if($row=mysqli_fetch_array($pa)){
			$nombre_empresa=$row['empresa'];
			$nit_empresa=$row['nit'];
			$dir_empresa=$row['direccion'];
			$tel_empresa=$row['tel'].'-'.$row['fax'];
			$pais_empresa=$row['pais'].' - '.$row['ciudad'];
		}
					
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Consultorio Medico</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="../../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="../../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
	

	
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../usuarios/perfil.php"><?php echo $_SESSION['user_name']; ?></a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;">Consultorio: <?php echo $nombre_Consultorio; ?> :: Fecha de Acceso : <?php echo fecha(date('Y-m-d')); ?> &nbsp;<a href="../../php_cerrar.php" class="btn btn-danger square-btn-adjust">Salir</a> </div>
        </nav>   
           <?php include_once "../../menu/m_pacientes.php"; ?>
        <div id="page-wrapper" >
            <div id="page-inner">						                
                 <!-- /. ROW  -->              			 
				 <center><button onclick="imprimir();" class="btn btn-success"><i class=" fa fa-print "></i> Imprimir</button></center><br>
				 <div id="imprimeme">
				 <div class="table-responsive">	
				<table  width="100%" style="border: 1px solid #660000; -moz-border-radius: 12px;-webkit-border-radius: 12px;padding: 10px;">
                 <tr>
                    <td>
						<center>
	                    <img src="../../img/logo.jpg" width="75px" height="75px"><br>
	                    <!--<strong><?php echo $nombre_empresa; ?></strong><br>-->
	                    </center>                                                    
                    </td>
                    <td>
					<td align="center">                     
                        <div style="font-size: 25px;"><strong><em><?php echo $nombre_medico; ?></em></strong></div>
                        <div style="font-size: 14px;"><strong>ORTODISTA Y TRAUMATOLOGA</strong></div>
                                    <strong>JVPM 7511</strong><br>
                                Post-grado Hospital Docente Universitario
                                     Dr. Dario Contreras, R.D.<br>
                        <!--<strong><?php echo $nombre_empresa; ?></strong><br>-->                                                 
                    </td>                                                  
                    </td>
                    <td>
                    	<center>
	                    <img src="../../img/logo_dos.png" width="75px" height="75px"><br>
	                    </center> 
                    </td>
                 </tr>                       	
                </table>
				</div>
                <hr/>



				<?php
					if(!empty($_POST['id_paciente'])){ 												
						$id_paciente=limpiar($_POST['id_paciente']);
						$sintomas=limpiar($_POST['sintomas']);
						$examen=limpiar($_POST['examen']);																												
						$diagnostico=limpiar($_POST['diagnostico']);																											
						$tratamiento=limpiar($_POST['tratamiento']);																											
						$reseta='ok';																											
						$observaciones=limpiar($_POST['observaciones']);
						$fecha=date('Y-m-d');
						//$hora=date('H:i:s');
						$status='PENDIENTE';
					#lOS MEDICAMENTOS
						$med1=limpiar($_POST['med1']);										
						$indi1=limpiar($_POST['indi1']);
						$med2=limpiar($_POST['med2']);										
						$indi2=limpiar($_POST['indi2']);
						$med3=limpiar($_POST['med3']);										
						$indi3=limpiar($_POST['indi3']);
						$med4=limpiar($_POST['med4']);										
						$indi4=limpiar($_POST['indi4']);
						$med5=limpiar($_POST['med5']);										
						$indi5=limpiar($_POST['indi5']);
						$med6=limpiar($_POST['med6']);										
						$indi6=limpiar($_POST['indi6']);
						$med7=limpiar($_POST['med7']);										
						$indi7=limpiar($_POST['indi7']);
						$med7=limpiar($_POST['med7']);										
						$indi7=limpiar($_POST['indi7']);
						$med8=limpiar($_POST['med8']);										
						$indi8=limpiar($_POST['indi8']);
						$med9=limpiar($_POST['med9']);										
						$indi9=limpiar($_POST['indi9']);
						$med10=limpiar($_POST['med10']);										
						$indi10=limpiar($_POST['indi10']);	
																	
					if(empty($_POST['id'])){
						$oConsulta=new Proceso_Consulta($conexion,'',$id_paciente,$usu,$id_consultorio,$sintomas,$examen,$diagnostico,$tratamiento,$reseta,$observaciones,$fecha,$hora,$status,
															$med1,$indi1,$med2,$indi2,$med3,$indi3,$med4,$indi4,$med5,$indi5,$med6,$indi6,$med7,$indi7,$med8,$indi8,$med9,$indi9,$med10,$indi10);
						$oConsulta->crear();
						echo mensajes('Consulta Medica Creada con Exito','verde');
					}else{
						$id=limpiar($_POST['id']);
						$oConsulta=new Proceso_Consulta($conexion,$id,$sintomas,$examen,$diagnostico,$tratamiento,$reseta,$observaciones);
						$oConsulta->actualizar();
						echo mensajes('Consulta Medica Actualizada con Exito','verde');
					}
				}
				?>
                <div style="font-size: 12px;">
                    	<strong>PACIENTE: </strong><?php echo $nombre; ?><br>
	                    <strong>FECHA: </strong><?php echo fecha($fecha); ?> ||  
	                    <strong>HORA: </strong><?php echo date($hora); ?><br>
	                    <strong>USUARIO: </strong><?php echo $cajero_nombre; ?>
						</div>
						<hr/>
                        <div style="font-size: 14px;"align="center">
                         <strong>PERFIL DEL PACIENTE</strong><br>                              
		                </div> 
					 	<button type="button" class="btn btn-info" data-toggle="modal"  data-target="#new">Consulta</button>
		                <hr/>				
                    <!-- /. TABLA  -->									
				<div class="col-md-6 col-sm-6">
				<div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" width="100%" style="font-size: 12px; border: 1px solid #660000; -moz-border-radius: 12px;-webkit-border-radius: 12px;padding: 10px;">
			                 <tr align="center">
			                 <td colspan="2"><strong>INFORMACIÓN PERSONAL</strong></td>
			                 </tr>
			                 <tr>
			                    <td>
			                    <!-- Advanced Tables -->
										<img src="data:image/jpg;base64,<?php echo base64_encode($imagen);?>" alt="" height="200px">                                           				
									    <strong>PACIENTE: </strong><?php echo $nombre; ?><br><br>
										<strong>DIRECCION: </strong><?php echo $direccion; ?><br><br>                               
										<strong>TEL: </strong><?php echo $telefono; ?><br><br>                                   
										<strong>EDAD: </strong><?php echo CalculaEdad($edad); ?> AÑOS<br><br>                                   
										<strong>SEXO: </strong><?php echo sexo($sexo); ?><br><br>                                   
										<strong>EMAIL: </strong><?php echo $email; ?><br><br>                                   
										<strong>ESTADO: </strong><?php echo estado($estado); ?><br><br> 									                  																												
																				                                                                      
			                    </td>
			                    </tr>			                  
			                    </table>	
                            </div>                                                                                         
                </div>
                <div class="col-md-6 col-sm-6">
				<div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" width="100%" style="font-size: 12px; border: 1px solid #660000; -moz-border-radius: 12px;-webkit-border-radius: 12px;padding: 10px;">
			                 <tr align="center">
			                 <td colspan="2"><strong>CUADRO CLINICO</strong></td>
			                 </tr>
			                 <tr>
			                    <td>
			                    <!-- Advanced Tables -->                                           				
									    <strong>TIPO DE SANGRE: </strong><?php echo $sangre; ?><br><br>
										<strong>VIH: </strong><?php echo $vih; ?><br><br>
										<strong>PESO: </strong><?php echo $peso; ?><br><br>                                   
										<strong>TALLA: </strong><?php echo $talla; ?><br><br>                                   
										<strong>ALERGIAS: </strong><?php echo $alergia; ?><br><br>                                   
										<strong>MEDICAMENTO: </strong><?php echo $medicamento; ?><br><br>                                   
										<strong>ENFERMEDAD: </strong><?php echo $enfermedad; ?><br><br>     									                  																												
																				                                                                      
			                    </td>
			                    </tr>			                  
			                    </table>	
                            </div>                                                                                         
                </div><br>
				
				<div class="col-md-12 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" width="100%" style="font-size: 12px; border: 1px solid #660000; -moz-border-radius: 12px;-webkit-border-radius: 12px;padding: 10px;">
						<caption><strong>HISTORIAL</strong></caption>
							<thead>
                                <tr>
                                    <th>SINTOMAS</th>
                                    <th>EXAMEN</th>
									<th>DIAGNOSTICO</th>
									<th>TRATAMIENTO</th> 
									<th>RESETA</th>                                                                                     
                                    <th>OBSERVACIONES</th>
									<th>FECHA</th>
									<th>HORA</th>
									<th></th>
                                </tr>
                            </thead>
							<tbody>
								<?php 
									$consulta="SELECT sintomas, examen, diagnostico, tratamiento, reseta, observaciones, fecha, hora FROM consultas_medicas WHERE id_paciente = '$idP' ORDER BY fecha,  hora";
									if($pamex=mysqli_query($conexion,$consulta)){	
										while($row=mysqli_fetch_array($pamex)){
								?>
								<tr>
									<td><?php echo $row['sintomas']; ?></td>
                                	<td><?php echo $row['examen']; ?></td>
									<td><?php echo $row['diagnostico']; ?></td>
									<td><?php echo $row['tratamiento']; ?></td>
									<td><?php echo $row['reseta']; ?></td>
									<td><?php echo $row['observaciones']; ?></td>
									<td><?php echo $row['fecha']; ?></td>
									<td><?php echo $row['hora']; ?></td>
									<td>
										<a href="../imprimir/index_two.php?id=<?php echo $factura; ?>" class="btn btn-primary btn-sm" title="Imprimir">
											<i class="fa fa-print" ></i>
										</a>
									</td>  
								</tr>

								<?php
										}
									}
								?>				
							</tbody>
						</table>
					</div>
				</div>
                <div style="font-size: 10px;" align="center">
										<strong><?php echo $nombre_empresa; ?></strong><br>
										<strong><?php echo $tel_empresa; ?></strong><br>
										<strong><?php echo $pais_empresa; ?></strong><br>
										<strong><?php echo $dir_empresa; ?></strong><br>
				</div>
				
				
				<!--  Modals-->
				<div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<form name="form1" method="post" action="">
						<input type="hidden" name="id_paciente" value="<?php echo $factura; ?>">
							<div class="modal-dialogx">
								<div class="modal-content">
									<div class="panel-body">
										<div class="row">
											<ul class="nav nav-tabs nav-justified">
	                                        	<li class="active"><a href="#datos" data-toggle="tab"><i class="glyphicon glyphicon-book" ></i> CONSULTA</a></li>
	                                        	<li class="" ><a href="#tipo" data-toggle="tab"><i class="glyphicon glyphicon-book" ></i> RECETA/MEDICAMENTOS</a></li>                                                                                                                                                                                     
	                                        </ul><br>
	                                        <div class="tab-content">
	                                            <div class="tab-pane fade active in" id="datos">
	                                            	<div class="col-md-6">
														<label>Nombre:</label> <?php echo $nombre; ?><br>													
														<label>Dirección:</label> <?php echo $direccion; ?><br>												
														<label>Telefono:</label> <?php echo $telefono; ?><br>
													</div>
													<div class="col-md-6">
														<?php
														   // primero conectamos siempre a la base de datos mysqli
															$sql = "SELECT * FROM citas_medicas WHERE consultorio='$id_consultorio' and id_paciente='$factura'";  // sentencia sql
															$result = mysqli_query($conexion,$sql);
															$numero = mysqli_num_rows($result); // obtenemos el número de filas
																		
														?>
														<label>Fecha de Nacimiento:</label> <?php echo fecha($edad); ?><br>														
														<label>Edad:</label> <?php echo CalculaEdad($edad); ?> Años<br>
														<label>Visitas: <span class="label label-success"><?php echo "$numero" ?></span></label><br><br>
													</div> 																																		   
													<div class="col-md-6">																						
														<span class="input-group-addon">Motivo de consulta:</span>
														<textarea class="form-control" name="sintomas" rows="3" required></textarea><br>
														<span class="input-group-addon">Examen Físico:</span>
														<textarea class="form-control" name="examen" rows="3" required></textarea><br>												
														<span class="input-group-addon">Diagnostico:</span>
														<textarea class="form-control" name="diagnostico" rows="3" required></textarea><br>
													</div>
													<div class="col-md-6">																				
														<span class="input-group-addon">Tratamiento:</span>
														<textarea class="form-control" name="tratamiento" rows="3" required></textarea><br>
														<span class="input-group-addon">Nota:</span>
														<textarea class="form-control" name="observaciones" rows="3"></textarea><br>																									
													</div>
												</div>
												<div class="tab-pane fade" id="tipo">
                                                    <div class="col-md-6">											
														<input class="form-control" name="med1" placeholder="Medicamento 1" autocomplete="off" required>
                                                   	 	<textarea class="form-control" name="indi1" placeholder="Indicación" rows="2" required></textarea><br>
                                                    	<input class="form-control" name="med2" placeholder="Medicamento 2" autocomplete="off">
                                                    	<textarea class="form-control" name="indi2" placeholder="Indicación" rows="2" ></textarea><br>
                                                    	<input class="form-control" name="med3" placeholder="Medicamento 3" autocomplete="off">
                                                    	<textarea class="form-control" name="indi3" placeholder="Indicación" rows="2" ></textarea><br>
                                                    	<input class="form-control" name="med4" placeholder="Medicamento 4" autocomplete="off">
                                                    	<textarea class="form-control" name="indi4" placeholder="Indicación" rows="2"></textarea><br>
                                                    	<input class="form-control" name="med5" placeholder="Medicamento 5" autocomplete="off">
                                                    	<textarea class="form-control" name="indi5" placeholder="Indicación" rows="2"></textarea><br>
													</div>
													<div class="col-md-6">																				
														<input class="form-control" name="med6" placeholder="Medicamento 6" autocomplete="off">
                                                    	<textarea class="form-control" name="indi6" placeholder="Indicación" rows="2"></textarea><br>
                                                    	<input class="form-control" name="med7" placeholder="Medicamento 7" autocomplete="off">
                                                    	<textarea class="form-control" name="indi7" placeholder="Indicación" rows="2" ></textarea><br>
                                                    	<input class="form-control" name="med8" placeholder="Medicamento 8" autocomplete="off">
                                                    	<textarea class="form-control" name="indi8" placeholder="Indicación" rows="2"></textarea><br>
                                                    	<input class="form-control" name="med9" placeholder="Medicamento 9" autocomplete="off">
                                                    	<textarea class="form-control" name="indi9" placeholder="Indicación" rows="2"></textarea><br>
                                                    	<input class="form-control" name="med10" placeholder="Medicamento 10" autocomplete="off">
                                                    	<textarea class="form-control" name="indi10" placeholder="Indicación" rows="2"></textarea><br>																							
													</div> 
												</div>
											</div>                                                                         																																												 																																													 
										</div> 
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
											<button type="submit" class="btn btn-primary">Guardar</button>
										</div>										 
								</div>
							</div>
					</form>
				</div>
					<!-- End Modals-->	 
						
			 </div>
			</div>
        </div>               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="../../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<!-- VALIDACIONES -->
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
	<script>
		function imprimir(){
		  var objeto=document.getElementById('imprimeme');  //obtenemos el objeto a imprimir
		  var ventana=window.open('','_blank');  //abrimos una ventana vacía nueva
		  ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
		  ventana.document.close();  //cerramos el documento
		  ventana.print();  //imprimimos la ventana
		  ventana.close();  //cerramos la ventana
		}
		function startTime(){
			today=new Date();
			h=today.getHours();
			m=today.getMinutes();
			s=today.getSeconds();
			m=checkTime(m);
			s=checkTime(s);
			document.getElementById('reloj').innerHTML=h+":"+m+":"+s;
			t=setTimeout('startTime()',500);}
		function checkTime(i)
				{if (i<10) {i="0" + i;}return i;}
				window.onload=function(){
					startTime();}
	</script>
    
   
</body>
</html>
