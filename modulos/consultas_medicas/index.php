<?php 
	session_start();
	include_once "../php_conexion.php";
	include_once "class/class.php";
	include_once "class/class_pago.php";
	include_once "../funciones.php";
	include_once "../class_buscar.php";
	#$documento=limpiar($_SESSION['cod_user']);
	if($_SESSION['cod_user']){
	}else{
		header('Location: ../../php_cerrar.php');
	}
	$id_medico=$_SESSION['cod_user'];
	$usu=$_SESSION['cod_user'];
	$oPersona=new Consultar_Cajero($conexion,$usu);
	$cajero_nombre=$oPersona->consultar('nom');
	
	
	$usu=$_SESSION['cod_user'];
	$pa=mysqli_query($conexion,"SELECT * FROM cajero WHERE usu='$usu'");				
	while($row=mysqli_fetch_array($pa)){
		$id_consultorio=$row['consultorio'];
		$oConsultorio=new Consultar_Deposito($conexion,$id_consultorio);
		$nombre_Consultorio=$oConsultorio->consultar('nombre');
	}
	
	######### TRAEMOS LOS DATOS DE LA EMPRESA #############
		$pa=mysqli_query($conexion,"SELECT * FROM empresa WHERE id=1");				
        if($row=mysqli_fetch_array($pa)){
			$nombre_empresa=$row['empresa'];
		}
		######### TRAEMOS LOS DATOS DE CONSULTORIO #############
		$pax=mysqli_query($conexion,"SELECT * FROM consultorios WHERE id=$id_consultorio");				
        if($row=mysqli_fetch_array($pax)){
			$nombre_medico=$row['encargado'];
			
		}		
	
	if(!empty($_GET['del'])){
		$id=$_GET['del'];
		mysqli_query($conexion,"DELETE FROM consultas_medicas WHERE id='$id'");
		header('index.php');
		
	}
	
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $nombre_empresa; ?></title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="../../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="../../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../usuarios/perfil.php"><?php echo $_SESSION['user_name']; ?></a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;">Consultorio: <?php echo $nombre_Consultorio; ?> :: Fecha de Acceso : <?php echo fecha(date('Y-m-d')); ?> &nbsp; <a href="../../php_cerrar.php" class="btn btn-danger square-btn-adjust">Salir</a> </div>
        </nav>   
           <?php include_once "../../menu/m_consulta_medica.php"; ?>
        <div id="page-wrapper" >
            <div id="page-inner">						                
							 <div class="table-responsive">
							 <?php 
									if(!empty($_POST['id_paciente'])){ 												
										$id_paciente=limpiar($_POST['id_paciente']);
										$sintomas=limpiar($_POST['sintomas']);
										$examen=limpiar($_POST['examen']);																												
										$diagnostico=limpiar($_POST['diagnostico']);																											
										$tratamiento=limpiar($_POST['tratamiento']);																											
										$reseta='ok';																											
										$observaciones=limpiar($_POST['observaciones']);
										$fecha=date('Y-m-d');
										$hora=date('H:i:s');
										$status='PENDIENTE';
										#lOS MEDICAMENTOS
										$med1=limpiar($_POST['med1']);										
										$indi1=limpiar($_POST['indi1']);
										$med2=limpiar($_POST['med2']);										
										$indi2=limpiar($_POST['indi2']);
										$med3=limpiar($_POST['med3']);										
										$indi3=limpiar($_POST['indi3']);
										$med4=limpiar($_POST['med4']);										
										$indi4=limpiar($_POST['indi4']);
										$med5=limpiar($_POST['med5']);										
										$indi5=limpiar($_POST['indi5']);
										$med6=limpiar($_POST['med6']);										
										$indi6=limpiar($_POST['indi6']);
										$med7=limpiar($_POST['med7']);										
										$indi7=limpiar($_POST['indi7']);
										$med7=limpiar($_POST['med7']);										
										$indi7=limpiar($_POST['indi7']);
										$med8=limpiar($_POST['med8']);										
										$indi8=limpiar($_POST['indi8']);
										$med9=limpiar($_POST['med9']);										
										$indi9=limpiar($_POST['indi9']);
										$med10=limpiar($_POST['med10']);										
										$indi10=limpiar($_POST['indi10']);	
																						
										if(empty($_POST['id'])){
											$oConsulta=new Proceso_Consulta($conexion,'',$id_paciente,$id_medico,$id_consultorio,$sintomas,$examen,$diagnostico,$tratamiento,$reseta,$observaciones,$fecha,$hora,$status,
																				$med1,$indi1,$med2,$indi2,$med3,$indi3,$med4,$indi4,$med5,$indi5,$med6,$indi6,$med7,$indi7,$med8,$indi8,$med9,$indi9,$med10,$indi10);
											$oConsulta->crear();
											echo mensajes('Consulta Medica Creada con Exito','verde');
										}else{
											$id=limpiar($_POST['id']);
											$oConsulta=new Proceso_Consulta($conexion,$id,$sintomas,$examen,$diagnostico,$tratamiento,$reseta,$observaciones);
											$oConsulta->actualizar();
											echo mensajes('Consulta Medica Actualizada con Exito','verde');
										}
									}
									################## GUARDAR PAGO GENERAL ###############	
									if(!empty($_POST['id_gen_pago'])){ 												
										$id_gen_pago=limpiar($_POST['id_gen_pago']);
										$concepto='Operacion al contado';																																																						
										$clase='CONSULTA';																																																						
										$valor=limpiar($_POST['valor_recibido']);
										$tipo='GEN';
										$fecha=date('Y-m-d');
										$hora=date('H:i:s');
										$status='CANCELADO';
										######### SACAMOS EL VALOR MAXIMO DE LA FACTURA Y LE SUMAMOS UNO ##########
										$pa=mysqli_query($conexion,"SELECT MAX(factura)as maximo FROM factura");				
								        if($row=mysqli_fetch_array($pa)){
											if($row['maximo']==NULL){
												$factura='100000001';
											}else{
												$factura=$row['maximo']+1;
											}
										}	
																																
										if(empty($_POST['id'])){
											$oPago=new Proceso_Pago($conexion,'',$id_gen_pago,$concepto,$factura,$clase,$valor,$tipo,$fecha,$hora,$status,$usu,$id_consultorio);
											$oPago->crear();
											echo mensajes('Pago Realizado Con Exito <a href="../detalle/index.php?detalle='.$factura.'"  class="btn btn-default btn-sm" title="Detalle">
                                            <i class="fa fa-print" ></i>
                                            </a>','verde');
										}else{
											$id=limpiar($_POST['id']);
											$oPago=new Proceso_Pago($conexion,$id,$consulta,$examenes,$tratamiento,$observaciones);
											$oPago->actualizar();
											echo mensajes('Consulta General Actualizada con Exito','verde');
										}
									}
									
								?>
				          <table class="table table-striped table-bordered table-hover" id="dataTables-example">                                    
									<thead>
                                        <tr>
                                            <th># CITA</th>
                                            <th>PACIENTES QUE PASARAN CONSULTA HOY</th>                                                                                                                                                                                                                                                                                                                                                                                             
                                            <th>HORA</th>                                                                                                                                                                                                                                                                                                                                                                                             
                                            <th>PAGO</th> 
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php 
											$pame=mysqli_query($conexion,"SELECT * FROM citas_medicas WHERE  consultorio='$id_consultorio' and status='PENDIENTE' and consulta='PENDIENTE' ORDER BY id DESC");														
											while($row=mysqli_fetch_array($pame)){
											$url=$row['id'];											
												$oPaciente=new Consultar_Paciente($conexion,$row['id_paciente']);
												$url=$row['id'];
												$id_paciente=$row['id_paciente'];
										?>
                                        <tr>                                           
                                            <td><?php echo $row['id']; ?></td>                                                                                     
                                            <td>
											<a href="#new<?php echo $row['id_paciente']; ?>" role="button" class="btn btn-info btn-xs"  data-toggle="modal">
											<strong><?php echo $oPaciente->consultar('nombre'); ?></strong>
                                            </a>
											</td>
											  <td><?php echo $row['horario']; ?></td>   
                                            <td><?php echo status($row['status']); ?></td>
                                            
											 <!--  Modals-->
										 <div class="modal fade" id="new<?php echo $row['id_paciente']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												<form name="form1" method="post" action="">
												<input type="hidden" name="id_paciente" value="<?php echo $row['id_paciente']; ?>">
													<div class="modal-dialogx">
														<div class="modal-content">
												<div class="panel-body">
												<div class="row">
												<ul class="nav nav-tabs nav-justified">
	                                            <li class="active"><a href="#datos" data-toggle="tab"><i class="glyphicon glyphicon-book" ></i> CONSULTA</a></li>
	                                            <li class="" ><a href="#tipo" data-toggle="tab"><i class="glyphicon glyphicon-book" ></i> RECETA/MEDICAMENTOS</a></li>                                                                                                                                                                                     
	                                            </ul><br>
	                                            <div class="tab-content">
	                                            <div class="tab-pane fade active in" id="datos">
	                                            <div class="col-md-6">
													<label>Nombre:</label> <?php echo $oPaciente->consultar('nombre'); ?><br>													
													<label>Dirección:</label> <?php echo $oPaciente->consultar('direccion'); ?><br>												
													<label>Telefono:</label> <?php echo $oPaciente->consultar('telefono'); ?><br>
													</div>
													<div class="col-md-6">
														<?php
														   // primero conectamos siempre a la base de datos mysqli
															$sql = "SELECT * FROM citas_medicas WHERE consultorio='$id_consultorio' and id_paciente='$id_paciente'";  // sentencia sql
															$result = mysqli_query($conexion,$sql);
															$numero = mysqli_num_rows($result); // obtenemos el número de filas
																		
														?>
														<label>Fecha de Nacimiento:</label> <?php echo fecha($oPaciente->consultar('edad')); ?><br>														
														<label>Edad:</label> <?php echo CalculaEdad($oPaciente->consultar('edad')); ?> Años<br>
														<label>Visitas: <span class="label label-success"><?php echo "$numero" ?></span></label><br><br>
													</div> 																																		   
													<div class="col-md-6">																						
													<span class="input-group-addon">Motivo de consulta:</span>
													<textarea class="form-control" name="sintomas" rows="3" required></textarea><br>
													<span class="input-group-addon">Examen Físico:</span>
													<textarea class="form-control" name="examen" rows="3" required></textarea><br>												
													<span class="input-group-addon">Diagnostico:</span>
													<textarea class="form-control" name="diagnostico" rows="3" required></textarea><br>
													</div>
													<div class="col-md-6">																				
														<span class="input-group-addon">Tratamiento:</span>
														<textarea class="form-control" name="tratamiento" rows="3" required></textarea><br>
														<span class="input-group-addon">Nota:</span>
														<textarea class="form-control" name="observaciones" rows="3"></textarea><br>																									
													</div>
													</div>
													<div class="tab-pane fade" id="tipo">
                                                    <div class="col-md-6">											
													<input class="form-control" name="med1" placeholder="Medicamento 1" autocomplete="off" required>
                                                    <textarea class="form-control" name="indi1" placeholder="Indicación" rows="2" required></textarea><br>
                                                    <input class="form-control" name="med2" placeholder="Medicamento 2" autocomplete="off">
                                                    <textarea class="form-control" name="indi2" placeholder="Indicación" rows="2" ></textarea><br>
                                                    <input class="form-control" name="med3" placeholder="Medicamento 3" autocomplete="off">
                                                    <textarea class="form-control" name="indi3" placeholder="Indicación" rows="2" ></textarea><br>
                                                    <input class="form-control" name="med4" placeholder="Medicamento 4" autocomplete="off">
                                                    <textarea class="form-control" name="indi4" placeholder="Indicación" rows="2"></textarea><br>
                                                    <input class="form-control" name="med5" placeholder="Medicamento 5" autocomplete="off">
                                                    <textarea class="form-control" name="indi5" placeholder="Indicación" rows="2"></textarea><br>
													</div>
													<div class="col-md-6">																				
													<input class="form-control" name="med6" placeholder="Medicamento 6" autocomplete="off">
                                                    <textarea class="form-control" name="indi6" placeholder="Indicación" rows="2"></textarea><br>
                                                    <input class="form-control" name="med7" placeholder="Medicamento 7" autocomplete="off">
                                                    <textarea class="form-control" name="indi7" placeholder="Indicación" rows="2" ></textarea><br>
                                                    <input class="form-control" name="med8" placeholder="Medicamento 8" autocomplete="off">
                                                    <textarea class="form-control" name="indi8" placeholder="Indicación" rows="2"></textarea><br>
                                                    <input class="form-control" name="med9" placeholder="Medicamento 9" autocomplete="off">
                                                    <textarea class="form-control" name="indi9" placeholder="Indicación" rows="2"></textarea><br>
                                                    <input class="form-control" name="med10" placeholder="Medicamento 10" autocomplete="off">
                                                    <textarea class="form-control" name="indi10" placeholder="Indicación" rows="2"></textarea><br>																							
													</div> 
													</div>
													</div>                                                                         																																												 																																													 
												</div> 
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
													<button type="submit" class="btn btn-primary">Guardar</button>
												</div>										 
												</div>
											</div>
											</form>
										</div>
								 <!-- End Modals-->
                                        </tr>
<?php } ?>
                                    </tbody>									
                                </table>
								 </div>					 
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                             ULTIMAS CONSULTAS
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">								
                                <table class="table table-striped table-bordered table-hover" <!--id="dataTables-example"-->
                                    
									<thead>
                                        <tr>
                                            <th>#</th>
                                            <th>PACIENTE</th>                                                                                                                              
                                            <th>FECHA</th>                                                                                      
                                            <th>MEDICO</th>                                                                                      
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php 											
											$pame=mysqli_query($conexion,"SELECT * FROM consultas_medicas WHERE consultorio='$id_consultorio' ORDER BY id DESC LIMIT 10");		 													
											while($row=mysqli_fetch_array($pame)){
											$oPaciente=new Consultar_Paciente($conexion,$row['id_paciente']);
							               
											$url=$row['id'];
											$id_gen_pago=$row['id_paciente'];
											$status=$row['status'];
											if ($status == "PENDIENTE"){
												$pago='<a href="#" data-toggle="modal" data-target="#pago'.$id_gen_pago.'" class="btn btn-sm btn-danger" title="PAGO">
															<i class="fa fa-dollar" ></i>
														    </a>';
															
												}
												else {
												$pago='<a href="#" data-toggle="modal" data-target="#pago" class="btn btn-sm btn-success" title="PAGADO">
															<i class="fa fa-star" ></i>
														    </a>';
												}

										?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $row['id']; ?></td>
                                            <td><i class="fa fa-user fa-2x"></i> <?php echo $oPaciente->consultar('nombre'); ?></td>                                                                                   
                                            <td><?php echo fecha($row['fecha']).' '.$row['hora']; ?></td>
											<td><?php echo $nombre_medico; ?></td>  
                                            <td class="center">
											<div class="btn-group">
											  <button data-toggle="dropdown" class="btn btn-warning btn-sm dropdown-toggle"><i class="fa fa-cog"></i> <span class="caret"></span></button>
											  <ul class="dropdown-menu  pull-right">
												<li><a href="#" data-toggle="modal" data-target="#actualizar<?php echo $row['id']; ?>"><i class="fa fa-edit"></i> Editar</a></li>
												<li class="divider"></li>
												<li><a href="#" data-toggle="modal" data-target="#actualizarx<?php echo $row['id']; ?>"><i class="glyphicon glyphicon-time"></i> Nueva Cita Medica</a></li>																																				
											  </ul>
											</div>
											<a href="../imprimir/index.php?id=<?php echo $url; ?>" class="btn btn-primary btn-sm" title="Imprimir">
											<i class="fa fa-print" ></i>
										    </a>
										    <?php echo $pago; ?>
											</td>								
                                        </tr> 																										
								<!--  Modals-->
										 <div class="modal fade" id="actualizar<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												<form name="form2" method="post" action="">
												<input type="hidden" name="id" value="<?php echo $row['id']; ?>">
													<div class="modal-dialogx">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>															
																<h4 align="center" class="modal-title" id="myModalLabel">MODIFICAR</h4>
															</div>
															<div class="panel-body">
															<div class="row">
																<div class="col-md-12">
																<div class="alert alert-info" align="center"><strong><?php echo $oPaciente->consultar('nombre'); ?></strong></div>
																<input type="hidden" name="nombre">																				
																</div>
																<div class="col-md-6">
																		<br>
																		<span class="input-group-addon">Motivo de consulta</span>
																		<textarea class="form-control" name="sintomas" value="<?php echo $row['sintomas']; ?>" rows="3"><?php echo $row['sintomas']; ?></textarea><br>
																		<span class="input-group-addon">Examen Físico</span>
																		<textarea class="form-control" name="examen" value="<?php echo $row['examen']; ?>" rows="3"><?php echo $row['examen']; ?></textarea><br>																	
																		<span class="input-group-addon">Diagnostico</span>
																		<textarea class="form-control" name="diagnostico" value="<?php echo $row['diagnostico']; ?>" rows="3"><?php echo $row['diagnostico']; ?></textarea><br>
																</div>
																<div class="col-md-6">
																		<br>
																		<span class="input-group-addon">Tratamiento</span>
																		<textarea class="form-control" name="tratamiento" value="<?php echo $row['tratamiento']; ?>" rows="3"><?php echo $row['tratamiento']; ?></textarea><br>																	
																	   	<span class="input-group-addon">Nota</span>
																		<textarea class="form-control" name="observaciones" value="<?php echo $row['observaciones']; ?>" rows="3"><?php echo $row['observaciones']; ?></textarea>											
																</div>                                                                        
															</div> 
															</div> 
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
																<button type="submit" class="btn btn-primary">Guardar</button>
															</div>										 
														</div>
													</div>
													</form>
												</div>
										 <!-- End Modals-->
										  <!--  Modals-->
			                                 <div class="modal fade" id="pago<?php echo $id_gen_pago; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                                <form name="contado" action="" method="post">
			                                <input type="hidden" value="<?php echo $id_gen_pago; ?>" name="id_gen_pago">
			                                <div class="modal-dialog">
			                                    <div class="modal-content">
			                                                    <div class="modal-header">
			                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                                        <h3 align="center" class="modal-title" id="myModalLabel"><?php echo $oPaciente->consultar('nombre'); ?></h3>
			                                                    </div>
			                                        <div class="panel-body">
			                                        <div class="row" align="center">
			                                        <div class="col-md-12 col-sm-12 col-xs-12">
			                                        </div>                                       
			                                            <br>
			                                              <div class="col-md-3">
			                                               </div>
			                                              <div class="col-md-6">			                 
			                                             <strong>Pago Por: </strong><br>
														<select class="form-control" name="valor_recibido" value="<?php echo $row['ref']; ?>">
		                                                	<?php
																$sal=mysqli_query($conexion,"SELECT * FROM tarifas WHERE estado='s'");				
																while($col=mysqli_fetch_array($sal)){
																	echo '<option value="'.$col['valor'].'">'.$col['nombre'].formato($col['valor']).'</option>';
																}
															?>
		                                                </select>                                           
			                                            <!--<input type="hidden" value="<?php echo $neto; ?>" name="valor_recibido">-->                 
			                                           </div>                                                                                                              
			                                        </div> 
			                                        </div> 
			                                        <div class="modal-footer">
			                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			                                            <button type="submit" class="btn btn-primary">Procesar</button>
			                                        </div>                                       
			                                    </div>
			                                </div>
			                                </form>
			                            </div>
			                     <!-- End Modals-->	         
										
											<?php } ?>
                                    </tbody>
									
                                </table>
								
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->		
        </div>               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="../../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../../assets/js/dataTables/dataTables.bootstrap.js"></script>
	<!-- VALIDACIONES -->
	<script src="../../assets/js/jasny-bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../../assets/js/custom.js"></script>
    
   
</body>
</html>
