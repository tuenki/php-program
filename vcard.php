
<?php
include_once "modulos/php_conexion.php";
include_once "modulos/pacientes/vcards/VCardParser.php";
include_once "modulos/funciones.php";
include_once "modulos/pacientes/class/class.php";
use JeroenDesloovere\VCard\VCardParser;




function SeRepite($FNewPaciente,$FPacientes){
	$Repite = false;
	foreach($FPacientes as $FPaciente){
		if($FNewPaciente["NombreCompleto"]==$FPaciente["NombreCompleto"]){
			$Repite=true;
			break;
		}
	}
	return $Repite;
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>VCards</title>
	<link href="assets/css/bootstrap.css" rel="stylesheet" />

</head>
<body>
	<div class="row" >
		<div class="col-md-3"></div>
<div class="col-md-6">
	<?php
			$arr = array();
			if(isset($_GET['ruta']))
			{

				//obtiene la ruta
				$ruta = $_GET['ruta'];
				if(is_dir($ruta)){
					if($dir = opendir($ruta)){
						while(($archivo = readdir($dir)) !== false){
							if($archivo != "." && $archivo != "..")
							{
								if(mb_stristr($archivo,".vcf")){
									//echo $archivo."<br>";
									array_push($arr, $archivo);
								}
							}
						}
					}
				}
				else{
					echo mensajes('Ruta incorrecta','rojo');
				}
							
			}			
		?>
	<?php 
			if(isset($_POST['Leer'])){
				$cntrepetido = 0;
				$cntNoRepetido= 0;
				$cntCreados = 0;
				$contarr = count($arr);
				$Pacientes = array();
				for ($i=0; $i < $contarr ; $i++) 
				{
					$pathToVCardExample = $ruta."/".$arr[$i];
					$parser = VCardParser::parseFromFile($pathToVCardExample);
					foreach($parser as $vcard) 
					{
					    $lastname = limpiar(@$vcard->lastname);
					    $firstname = limpiar(@$vcard->firstname);
						$number = @$vcard->phone;
						$mail = @$vcard->email['HOME'][0];
						$address= @$vcard->address['HOME'][0];
					    $img = @$vcard->rawPhoto;
					    $notamot = @$vcard->note;
						$calle = @$address->street;
						$firstname = trim($firstname,"\t\n\r\0\x0B ");
						$lastname = trim($lastname,"\t\n\r\0\x0B ");
					    $nombrejunto = @$firstname." ". $lastname;
						$img = addslashes($img);
						$doc = limpiar($_POST['medico']);

						@$tel = "";
						if($number != null){
							@$keys = array_keys($number); 
							if(in_array($keys[0],$keys))
							{
								@$cell = $number[$keys[0]];
								@$tel = (string)$cell[0];
								$tel = trim($tel,"+");
								//echo $tel;
							}
						}
						//Crea un array asociativo

						$NewPaciente = ["NombreCompleto" => $nombrejunto,
										"Numero" => $tel,
										"Email" => $mail,
										"Direccion" => $calle,
										"Nota" => $notamot,
										"Imagen" => $img,
						];
						
						
						if(!SeRepite($NewPaciente,$Pacientes)){
							array_push($Pacientes,$NewPaciente);
						}
						//array_push($Pacientes,$NewPaciente);

						
					}
				}
				if(count($Pacientes) > 0){
					$pas = mysqli_query($conexion,"SELECT nombre FROM pacientes");
					foreach($Pacientes as $Paciente){
						$EsIgual = false;
						mysqli_data_seek($pas,0);
						while($row=mysqli_fetch_array($pas)){
							if($Paciente["NombreCompleto"]==$row['nombre']){
								$EsIgual=true;
								break;
							}
						}
						if(!$EsIgual){
							$oPaciente=new Proceso_Paciente($conexion,'',$Paciente["Imagen"],$doc,'',$Paciente["NombreCompleto"],$Paciente["Direccion"],$Paciente["Numero"],'','',$Paciente["Email"],'n',1,'','','',$Paciente["Nota"],'','','','');
							$oPaciente->crear();
							$cntCreados++;
						}
					}

				}
				if($cntCreados > 0)
					echo mensajes('Se han creado <b>'.$cntCreados.'</b> usuarios correctamente','verde');
				else
					echo mensajes('Todos los usuarios estan repetidos','rojo');
			}
		?>
	<form method="get" action="">
		<h4>Dirección de archivos</h4>
		<h5>Por defecto: archivos</h5>
		<input class="form-control input-lg" type="text" name="ruta" value="archivos"><br>
		<input class="btn btn-primary btn-sm " type="submit" value="Actualizar">
	</form>
	<div id="Contcarp">
		<br>
		<h4>Contenido:</h4>
		<?php
			$contarr = count($arr);
			for ($i=0; $i < $contarr ; $i++) { 
				# code...
				echo $arr[$i]. "<br>";
			}
		?>
		
	</div>
	<div>
		<hr>
			<form action="" method="post">
				<span class="input-group-addon">Asignar Medico:</span>
												 <select class="form-control" name="medico" id="medico" autocomplete="off" required>			
													<?php
														$p=mysqli_query($conexion,"SELECT doc, nombre FROM usuario WHERE estado='s'");				
														while($r=mysqli_fetch_array($p)){
															echo '<option value="'.$r['doc'].'">'.$r['nombre'].'</option>';
														}
													?>										
												</select>
			<input class="btn btn-success btn-sm " name="Leer" type="submit" value="Subir vCards">
		</form>
		
	</div>
</div>
</div>
</body>
</html>